﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//使用到messageBox类，因此加入下命名空间
using System.Windows.Forms;
//使用到ADO.net,因此加入以下两个命名空间
using System.Data;
using System.Data.SqlClient;
//使用到加密类，因此加入以下两个命名空间
using System.Security;
using System.Security.Cryptography;

using System.Web;
 
namespace 信息查询
{
    //更新,语句,修改  update
    //Insert 插入,入块,嵌件  insert
    //Delete 删除,删去,删除键  delete       Select 选择,挑选,选拔
    class sujuku
    {
        public static string strcon = @"Data Source=.;Initial Catalog=shixun;Integrated Security=True";
      
        public static string securet(String str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] palindata = Encoding.Default.GetBytes(str);//将要加密的字符串转换为字节数组
            byte[] encryptdata = md5.ComputeHash(palindata);//将字符串加密后也转换为字符数组
            return  Convert.ToBase64String(encryptdata);//将加密后的字节数组转换为加密字符串,并返回
        }
        public static string Encrypt(string strPwd)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.Default.GetBytes(strPwd);//将字符编码为一个字节序列 
            byte[] md5data = md5.ComputeHash(data);//计算data字节数组的哈希值 
            md5.Clear();
            string str = "";
            for (int i = 0; i < md5data.Length - 1; i++)
            {
                str += md5data[i].ToString("x").PadLeft(2, '0');
            }
            return str;
        } 

        //从数据库读数据，返回DataTable
        public static DataTable getDataFromDB(String sql)
        {
            DataTable dataT = new DataTable();
            try
            {
                SqlConnection conn = new SqlConnection(strcon);
                conn.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(sql, conn);
                sqlDa.Fill(dataT);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            if (dataT.Rows.Count > 0)
                return dataT;
            else
                return null;
        }
        //操纵数据库数据，可以是insert、update、delete
        public static bool updateDB(String sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = strcon;
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
        }
        //带事务处理的数据操纵方法
        public static bool ProUpdateDB(List<String> Sqls)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = strcon;
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;

            SqlTransaction tt;
            tt = conn.BeginTransaction();
            cmd.Transaction = tt;

            try
            {
                //执行集合Sqls中的多条数据操纵SQL语句
                for (int i = 0; i < Sqls.Count; i++)
                {
                    cmd.CommandText = Sqls[i];
                    cmd.ExecuteNonQuery();
                }
                tt.Commit();
                return true;
            }
            catch(Exception ex)
            {
                tt.Rollback();
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
    }

}
