﻿namespace 信息查询
{
    partial class 充值信息
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.账号DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.手机号DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.充值金额DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.时间DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.处理DataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.充值冻结BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.shixunDataSet2 = new 信息查询.shixunDataSet2();
            this.充值冻结TableAdapter = new 信息查询.shixunDataSet2TableAdapters.充值冻结TableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.充值冻结BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shixunDataSet2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.账号DataGridViewTextBoxColumn,
            this.手机号DataGridViewTextBoxColumn,
            this.充值金额DataGridViewTextBoxColumn,
            this.时间DataGridViewTextBoxColumn,
            this.处理DataGridViewCheckBoxColumn});
            this.dataGridView1.DataSource = this.充值冻结BindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 76);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(493, 174);
            this.dataGridView1.TabIndex = 0;
            // 
            // 账号DataGridViewTextBoxColumn
            // 
            this.账号DataGridViewTextBoxColumn.DataPropertyName = "账号";
            this.账号DataGridViewTextBoxColumn.HeaderText = "账号";
            this.账号DataGridViewTextBoxColumn.Name = "账号DataGridViewTextBoxColumn";
            this.账号DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 手机号DataGridViewTextBoxColumn
            // 
            this.手机号DataGridViewTextBoxColumn.DataPropertyName = "手机号";
            this.手机号DataGridViewTextBoxColumn.HeaderText = "手机号";
            this.手机号DataGridViewTextBoxColumn.Name = "手机号DataGridViewTextBoxColumn";
            this.手机号DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 充值金额DataGridViewTextBoxColumn
            // 
            this.充值金额DataGridViewTextBoxColumn.DataPropertyName = "充值金额";
            this.充值金额DataGridViewTextBoxColumn.HeaderText = "充值金额";
            this.充值金额DataGridViewTextBoxColumn.Name = "充值金额DataGridViewTextBoxColumn";
            this.充值金额DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 时间DataGridViewTextBoxColumn
            // 
            this.时间DataGridViewTextBoxColumn.DataPropertyName = "时间";
            this.时间DataGridViewTextBoxColumn.HeaderText = "时间";
            this.时间DataGridViewTextBoxColumn.Name = "时间DataGridViewTextBoxColumn";
            this.时间DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 处理DataGridViewCheckBoxColumn
            // 
            this.处理DataGridViewCheckBoxColumn.DataPropertyName = "处理";
            this.处理DataGridViewCheckBoxColumn.HeaderText = "处理";
            this.处理DataGridViewCheckBoxColumn.Name = "处理DataGridViewCheckBoxColumn";
            this.处理DataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // 充值冻结BindingSource
            // 
            this.充值冻结BindingSource.DataMember = "充值冻结";
            this.充值冻结BindingSource.DataSource = this.shixunDataSet2;
            // 
            // shixunDataSet2
            // 
            this.shixunDataSet2.DataSetName = "shixunDataSet2";
            this.shixunDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // 充值冻结TableAdapter
            // 
            this.充值冻结TableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(390, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "退回";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(267, 33);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "充值";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(153, 33);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "刷新";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // 充值信息
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 262);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "充值信息";
            this.Text = "充值信息";
            this.Load += new System.EventHandler(this.充值信息_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.充值冻结BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shixunDataSet2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private shixunDataSet2 shixunDataSet2;
        private System.Windows.Forms.BindingSource 充值冻结BindingSource;
        private shixunDataSet2TableAdapters.充值冻结TableAdapter 充值冻结TableAdapter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 账号DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 手机号DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 充值金额DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 时间DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 处理DataGridViewCheckBoxColumn;
        private System.Windows.Forms.Button button3;
    }
}