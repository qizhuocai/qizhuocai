﻿namespace 信息查询
{
    partial class 理财信息
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.shixunDataSet1 = new 信息查询.shixunDataSet1();
            this.理财管理BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.理财管理TableAdapter = new 信息查询.shixunDataSet1TableAdapters.理财管理TableAdapter();
            this.账号DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.投资项目DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.本息DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.利率DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.金额DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.操作时间DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.到期时间DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.是否回款DataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.冻结DataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shixunDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.理财管理BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.账号DataGridViewTextBoxColumn,
            this.投资项目DataGridViewTextBoxColumn,
            this.本息DataGridViewTextBoxColumn,
            this.利率DataGridViewTextBoxColumn,
            this.金额DataGridViewTextBoxColumn,
            this.操作时间DataGridViewTextBoxColumn,
            this.到期时间DataGridViewTextBoxColumn,
            this.是否回款DataGridViewCheckBoxColumn,
            this.冻结DataGridViewCheckBoxColumn});
            this.dataGridView1.DataSource = this.理财管理BindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 30);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(422, 220);
            this.dataGridView1.TabIndex = 0;
            // 
            // shixunDataSet1
            // 
            this.shixunDataSet1.DataSetName = "shixunDataSet1";
            this.shixunDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // 理财管理BindingSource
            // 
            this.理财管理BindingSource.DataMember = "理财管理";
            this.理财管理BindingSource.DataSource = this.shixunDataSet1;
            // 
            // 理财管理TableAdapter
            // 
            this.理财管理TableAdapter.ClearBeforeFill = true;
            // 
            // 账号DataGridViewTextBoxColumn
            // 
            this.账号DataGridViewTextBoxColumn.DataPropertyName = "账号";
            this.账号DataGridViewTextBoxColumn.HeaderText = "账号";
            this.账号DataGridViewTextBoxColumn.Name = "账号DataGridViewTextBoxColumn";
            // 
            // 投资项目DataGridViewTextBoxColumn
            // 
            this.投资项目DataGridViewTextBoxColumn.DataPropertyName = "投资项目";
            this.投资项目DataGridViewTextBoxColumn.HeaderText = "投资项目";
            this.投资项目DataGridViewTextBoxColumn.Name = "投资项目DataGridViewTextBoxColumn";
            // 
            // 本息DataGridViewTextBoxColumn
            // 
            this.本息DataGridViewTextBoxColumn.DataPropertyName = "本息";
            this.本息DataGridViewTextBoxColumn.HeaderText = "本息";
            this.本息DataGridViewTextBoxColumn.Name = "本息DataGridViewTextBoxColumn";
            // 
            // 利率DataGridViewTextBoxColumn
            // 
            this.利率DataGridViewTextBoxColumn.DataPropertyName = "利率";
            this.利率DataGridViewTextBoxColumn.HeaderText = "利率";
            this.利率DataGridViewTextBoxColumn.Name = "利率DataGridViewTextBoxColumn";
            // 
            // 金额DataGridViewTextBoxColumn
            // 
            this.金额DataGridViewTextBoxColumn.DataPropertyName = "金额";
            this.金额DataGridViewTextBoxColumn.HeaderText = "金额";
            this.金额DataGridViewTextBoxColumn.Name = "金额DataGridViewTextBoxColumn";
            // 
            // 操作时间DataGridViewTextBoxColumn
            // 
            this.操作时间DataGridViewTextBoxColumn.DataPropertyName = "操作时间";
            this.操作时间DataGridViewTextBoxColumn.HeaderText = "操作时间";
            this.操作时间DataGridViewTextBoxColumn.Name = "操作时间DataGridViewTextBoxColumn";
            // 
            // 到期时间DataGridViewTextBoxColumn
            // 
            this.到期时间DataGridViewTextBoxColumn.DataPropertyName = "到期时间";
            this.到期时间DataGridViewTextBoxColumn.HeaderText = "到期时间";
            this.到期时间DataGridViewTextBoxColumn.Name = "到期时间DataGridViewTextBoxColumn";
            // 
            // 是否回款DataGridViewCheckBoxColumn
            // 
            this.是否回款DataGridViewCheckBoxColumn.DataPropertyName = "是否回款";
            this.是否回款DataGridViewCheckBoxColumn.HeaderText = "是否回款";
            this.是否回款DataGridViewCheckBoxColumn.Name = "是否回款DataGridViewCheckBoxColumn";
            // 
            // 冻结DataGridViewCheckBoxColumn
            // 
            this.冻结DataGridViewCheckBoxColumn.DataPropertyName = "冻结";
            this.冻结DataGridViewCheckBoxColumn.HeaderText = "冻结";
            this.冻结DataGridViewCheckBoxColumn.Name = "冻结DataGridViewCheckBoxColumn";
            // 
            // 理财信息
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 262);
            this.Controls.Add(this.dataGridView1);
            this.Name = "理财信息";
            this.Text = "理财信息";
            this.Load += new System.EventHandler(this.理财信息_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shixunDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.理财管理BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private shixunDataSet1 shixunDataSet1;
        private System.Windows.Forms.BindingSource 理财管理BindingSource;
        private shixunDataSet1TableAdapters.理财管理TableAdapter 理财管理TableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn 账号DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 投资项目DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 本息DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 利率DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 金额DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 操作时间DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 到期时间DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 是否回款DataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 冻结DataGridViewCheckBoxColumn;
    }
}