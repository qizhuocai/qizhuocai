﻿namespace 信息查询
{
    partial class 贷款信息
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.账号DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.本息DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.贷款金额DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.利息DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.到期时间DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.贷款时间DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.贷款BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.shixunDataSet = new 信息查询.shixunDataSet();
            this.贷款TableAdapter = new 信息查询.shixunDataSetTableAdapters.贷款TableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.贷款BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shixunDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.账号DataGridViewTextBoxColumn,
            this.类型DataGridViewTextBoxColumn,
            this.本息DataGridViewTextBoxColumn,
            this.贷款金额DataGridViewTextBoxColumn,
            this.利息DataGridViewTextBoxColumn,
            this.到期时间DataGridViewTextBoxColumn,
            this.贷款时间DataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.贷款BindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 51);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(408, 199);
            this.dataGridView1.TabIndex = 0;
            // 
            // 账号DataGridViewTextBoxColumn
            // 
            this.账号DataGridViewTextBoxColumn.DataPropertyName = "账号";
            this.账号DataGridViewTextBoxColumn.HeaderText = "账号";
            this.账号DataGridViewTextBoxColumn.Name = "账号DataGridViewTextBoxColumn";
            this.账号DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 类型DataGridViewTextBoxColumn
            // 
            this.类型DataGridViewTextBoxColumn.DataPropertyName = "类型";
            this.类型DataGridViewTextBoxColumn.HeaderText = "类型";
            this.类型DataGridViewTextBoxColumn.Name = "类型DataGridViewTextBoxColumn";
            this.类型DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 本息DataGridViewTextBoxColumn
            // 
            this.本息DataGridViewTextBoxColumn.DataPropertyName = "本息";
            this.本息DataGridViewTextBoxColumn.HeaderText = "本息";
            this.本息DataGridViewTextBoxColumn.Name = "本息DataGridViewTextBoxColumn";
            this.本息DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 贷款金额DataGridViewTextBoxColumn
            // 
            this.贷款金额DataGridViewTextBoxColumn.DataPropertyName = "贷款金额";
            this.贷款金额DataGridViewTextBoxColumn.HeaderText = "贷款金额";
            this.贷款金额DataGridViewTextBoxColumn.Name = "贷款金额DataGridViewTextBoxColumn";
            this.贷款金额DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 利息DataGridViewTextBoxColumn
            // 
            this.利息DataGridViewTextBoxColumn.DataPropertyName = "利息";
            this.利息DataGridViewTextBoxColumn.HeaderText = "利息";
            this.利息DataGridViewTextBoxColumn.Name = "利息DataGridViewTextBoxColumn";
            this.利息DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 到期时间DataGridViewTextBoxColumn
            // 
            this.到期时间DataGridViewTextBoxColumn.DataPropertyName = "到期时间";
            this.到期时间DataGridViewTextBoxColumn.HeaderText = "到期时间";
            this.到期时间DataGridViewTextBoxColumn.Name = "到期时间DataGridViewTextBoxColumn";
            this.到期时间DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 贷款时间DataGridViewTextBoxColumn
            // 
            this.贷款时间DataGridViewTextBoxColumn.DataPropertyName = "贷款时间";
            this.贷款时间DataGridViewTextBoxColumn.HeaderText = "贷款时间";
            this.贷款时间DataGridViewTextBoxColumn.Name = "贷款时间DataGridViewTextBoxColumn";
            this.贷款时间DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 贷款BindingSource
            // 
            this.贷款BindingSource.DataMember = "贷款";
            this.贷款BindingSource.DataSource = this.shixunDataSet;
            // 
            // shixunDataSet
            // 
            this.shixunDataSet.DataSetName = "shixunDataSet";
            this.shixunDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // 贷款TableAdapter
            // 
            this.贷款TableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(314, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // 贷款信息
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 262);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "贷款信息";
            this.Text = "贷款";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.贷款BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shixunDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private shixunDataSet shixunDataSet;
        private System.Windows.Forms.BindingSource 贷款BindingSource;
        private shixunDataSetTableAdapters.贷款TableAdapter 贷款TableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn 账号DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 本息DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 贷款金额DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 利息DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 到期时间DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 贷款时间DataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
    }
}

