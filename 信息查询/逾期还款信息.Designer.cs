﻿namespace 信息查询
{
    partial class 逾期还款信息
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.逾期还款 = new 信息查询.逾期还款();
            this.贷款逾期BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.贷款逾期TableAdapter = new 信息查询.逾期还款TableAdapters.贷款逾期TableAdapter();
            this.账号DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.未还金额DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.应还金额DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.逾期金DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.应还时间DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.备注DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.逾期还款)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.贷款逾期BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.账号DataGridViewTextBoxColumn,
            this.未还金额DataGridViewTextBoxColumn,
            this.应还金额DataGridViewTextBoxColumn,
            this.逾期金DataGridViewTextBoxColumn,
            this.应还时间DataGridViewTextBoxColumn,
            this.备注DataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.贷款逾期BindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(-2, 51);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(522, 199);
            this.dataGridView1.TabIndex = 0;
            // 
            // 逾期还款
            // 
            this.逾期还款.DataSetName = "逾期还款";
            this.逾期还款.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // 贷款逾期BindingSource
            // 
            this.贷款逾期BindingSource.DataMember = "贷款逾期";
            this.贷款逾期BindingSource.DataSource = this.逾期还款;
            // 
            // 贷款逾期TableAdapter
            // 
            this.贷款逾期TableAdapter.ClearBeforeFill = true;
            // 
            // 账号DataGridViewTextBoxColumn
            // 
            this.账号DataGridViewTextBoxColumn.DataPropertyName = "账号";
            this.账号DataGridViewTextBoxColumn.HeaderText = "账号";
            this.账号DataGridViewTextBoxColumn.Name = "账号DataGridViewTextBoxColumn";
            this.账号DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 未还金额DataGridViewTextBoxColumn
            // 
            this.未还金额DataGridViewTextBoxColumn.DataPropertyName = "未还金额";
            this.未还金额DataGridViewTextBoxColumn.HeaderText = "未还金额";
            this.未还金额DataGridViewTextBoxColumn.Name = "未还金额DataGridViewTextBoxColumn";
            this.未还金额DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 应还金额DataGridViewTextBoxColumn
            // 
            this.应还金额DataGridViewTextBoxColumn.DataPropertyName = "应还金额";
            this.应还金额DataGridViewTextBoxColumn.HeaderText = "应还金额";
            this.应还金额DataGridViewTextBoxColumn.Name = "应还金额DataGridViewTextBoxColumn";
            this.应还金额DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 逾期金DataGridViewTextBoxColumn
            // 
            this.逾期金DataGridViewTextBoxColumn.DataPropertyName = "逾期金";
            this.逾期金DataGridViewTextBoxColumn.HeaderText = "逾期金";
            this.逾期金DataGridViewTextBoxColumn.Name = "逾期金DataGridViewTextBoxColumn";
            this.逾期金DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 应还时间DataGridViewTextBoxColumn
            // 
            this.应还时间DataGridViewTextBoxColumn.DataPropertyName = "应还时间";
            this.应还时间DataGridViewTextBoxColumn.HeaderText = "应还时间";
            this.应还时间DataGridViewTextBoxColumn.Name = "应还时间DataGridViewTextBoxColumn";
            this.应还时间DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 备注DataGridViewTextBoxColumn
            // 
            this.备注DataGridViewTextBoxColumn.DataPropertyName = "备注";
            this.备注DataGridViewTextBoxColumn.HeaderText = "备注";
            this.备注DataGridViewTextBoxColumn.Name = "备注DataGridViewTextBoxColumn";
            this.备注DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 逾期还款信息
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 262);
            this.Controls.Add(this.dataGridView1);
            this.Name = "逾期还款信息";
            this.Text = "逾期还款信息";
            this.Load += new System.EventHandler(this.逾期还款信息_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.逾期还款)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.贷款逾期BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private 逾期还款 逾期还款;
        private System.Windows.Forms.BindingSource 贷款逾期BindingSource;
        private 逾期还款TableAdapters.贷款逾期TableAdapter 贷款逾期TableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn 账号DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 未还金额DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 应还金额DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 逾期金DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 应还时间DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 备注DataGridViewTextBoxColumn;


    }
}