﻿namespace 天地银行管理系统
{
    partial class 开户信息
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button退出 = new System.Windows.Forms.Button();
            this.tb点击获取账号名 = new System.Windows.Forms.Button();
            this.tb开户 = new System.Windows.Forms.Button();
            this.textBox帐户名 = new System.Windows.Forms.TextBox();
            this.textBox输入密码 = new System.Windows.Forms.TextBox();
            this.textBox确认 = new System.Windows.Forms.TextBox();
            this.label确认 = new System.Windows.Forms.Label();
            this.label输入密码 = new System.Windows.Forms.Label();
            this.textBox电话 = new System.Windows.Forms.TextBox();
            this.label电话 = new System.Windows.Forms.Label();
            this.textBox身份证 = new System.Windows.Forms.TextBox();
            this.label身份证 = new System.Windows.Forms.Label();
            this.textBox姓名 = new System.Windows.Forms.TextBox();
            this.label姓名 = new System.Windows.Forms.Label();
            this.label开户帐户名 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button退出
            // 
            this.button退出.Location = new System.Drawing.Point(311, 191);
            this.button退出.Name = "button退出";
            this.button退出.Size = new System.Drawing.Size(75, 23);
            this.button退出.TabIndex = 5;
            this.button退出.Text = "退出";
            this.button退出.UseVisualStyleBackColor = true;
            this.button退出.Click += new System.EventHandler(this.button退出_Click);
            // 
            // tb点击获取账号名
            // 
            this.tb点击获取账号名.Location = new System.Drawing.Point(28, 180);
            this.tb点击获取账号名.Name = "tb点击获取账号名";
            this.tb点击获取账号名.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tb点击获取账号名.Size = new System.Drawing.Size(103, 23);
            this.tb点击获取账号名.TabIndex = 4;
            this.tb点击获取账号名.Text = "点击获取账号名";
            this.tb点击获取账号名.UseVisualStyleBackColor = true;
            this.tb点击获取账号名.Click += new System.EventHandler(this.tb点击获取账号名_Click);
            // 
            // tb开户
            // 
            this.tb开户.Location = new System.Drawing.Point(206, 191);
            this.tb开户.Name = "tb开户";
            this.tb开户.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tb开户.Size = new System.Drawing.Size(75, 23);
            this.tb开户.TabIndex = 6;
            this.tb开户.Text = "开户";
            this.tb开户.UseVisualStyleBackColor = true;
            this.tb开户.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox帐户名
            // 
            this.textBox帐户名.Location = new System.Drawing.Point(83, 43);
            this.textBox帐户名.Name = "textBox帐户名";
            this.textBox帐户名.Size = new System.Drawing.Size(139, 21);
            this.textBox帐户名.TabIndex = 12;
            this.textBox帐户名.Validating += new System.ComponentModel.CancelEventHandler(this.textBox帐户名_Validating);
            // 
            // textBox输入密码
            // 
            this.textBox输入密码.Location = new System.Drawing.Point(287, 79);
            this.textBox输入密码.Name = "textBox输入密码";
            this.textBox输入密码.Size = new System.Drawing.Size(139, 21);
            this.textBox输入密码.TabIndex = 16;
            this.textBox输入密码.UseSystemPasswordChar = true;
            // 
            // textBox确认
            // 
            this.textBox确认.Location = new System.Drawing.Point(287, 118);
            this.textBox确认.Name = "textBox确认";
            this.textBox确认.Size = new System.Drawing.Size(139, 21);
            this.textBox确认.TabIndex = 17;
            this.textBox确认.UseSystemPasswordChar = true;
            // 
            // label确认
            // 
            this.label确认.AutoSize = true;
            this.label确认.Location = new System.Drawing.Point(228, 118);
            this.label确认.Name = "label确认";
            this.label确认.Size = new System.Drawing.Size(53, 12);
            this.label确认.TabIndex = 23;
            this.label确认.Text = "确认密码";
            // 
            // label输入密码
            // 
            this.label输入密码.AutoSize = true;
            this.label输入密码.Location = new System.Drawing.Point(228, 84);
            this.label输入密码.Name = "label输入密码";
            this.label输入密码.Size = new System.Drawing.Size(53, 12);
            this.label输入密码.TabIndex = 22;
            this.label输入密码.Text = "输入密码";
            // 
            // textBox电话
            // 
            this.textBox电话.Location = new System.Drawing.Point(287, 40);
            this.textBox电话.Name = "textBox电话";
            this.textBox电话.Size = new System.Drawing.Size(139, 21);
            this.textBox电话.TabIndex = 15;
            this.textBox电话.Validating += new System.ComponentModel.CancelEventHandler(this.textBox电话_Validating);
            // 
            // label电话
            // 
            this.label电话.AutoSize = true;
            this.label电话.Location = new System.Drawing.Point(228, 49);
            this.label电话.Name = "label电话";
            this.label电话.Size = new System.Drawing.Size(29, 12);
            this.label电话.TabIndex = 21;
            this.label电话.Text = "电话";
            // 
            // textBox身份证
            // 
            this.textBox身份证.Location = new System.Drawing.Point(83, 117);
            this.textBox身份证.Name = "textBox身份证";
            this.textBox身份证.Size = new System.Drawing.Size(139, 21);
            this.textBox身份证.TabIndex = 14;
            // 
            // label身份证
            // 
            this.label身份证.AutoSize = true;
            this.label身份证.Location = new System.Drawing.Point(26, 121);
            this.label身份证.Name = "label身份证";
            this.label身份证.Size = new System.Drawing.Size(41, 12);
            this.label身份证.TabIndex = 20;
            this.label身份证.Text = "身份证";
            // 
            // textBox姓名
            // 
            this.textBox姓名.Location = new System.Drawing.Point(83, 80);
            this.textBox姓名.Name = "textBox姓名";
            this.textBox姓名.Size = new System.Drawing.Size(139, 21);
            this.textBox姓名.TabIndex = 13;
            // 
            // label姓名
            // 
            this.label姓名.AutoSize = true;
            this.label姓名.Location = new System.Drawing.Point(26, 84);
            this.label姓名.Name = "label姓名";
            this.label姓名.Size = new System.Drawing.Size(29, 12);
            this.label姓名.TabIndex = 19;
            this.label姓名.Text = "姓名";
            // 
            // label开户帐户名
            // 
            this.label开户帐户名.AutoSize = true;
            this.label开户帐户名.Location = new System.Drawing.Point(26, 52);
            this.label开户帐户名.Name = "label开户帐户名";
            this.label开户帐户名.Size = new System.Drawing.Size(41, 12);
            this.label开户帐户名.TabIndex = 18;
            this.label开户帐户名.Text = "帐户名";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 253);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(489, 22);
            this.statusStrip1.TabIndex = 24;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(131, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // 开户信息
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 275);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.textBox帐户名);
            this.Controls.Add(this.textBox输入密码);
            this.Controls.Add(this.textBox确认);
            this.Controls.Add(this.label确认);
            this.Controls.Add(this.label输入密码);
            this.Controls.Add(this.textBox电话);
            this.Controls.Add(this.label电话);
            this.Controls.Add(this.textBox身份证);
            this.Controls.Add(this.label身份证);
            this.Controls.Add(this.textBox姓名);
            this.Controls.Add(this.label姓名);
            this.Controls.Add(this.label开户帐户名);
            this.Controls.Add(this.tb开户);
            this.Controls.Add(this.button退出);
            this.Controls.Add(this.tb点击获取账号名);
            this.Name = "开户信息";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "开户信息";
            this.Load += new System.EventHandler(this.开户信息_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button退出;
        private System.Windows.Forms.Button tb点击获取账号名;
        private System.Windows.Forms.Button tb开户;
        private System.Windows.Forms.TextBox textBox帐户名;
        private System.Windows.Forms.TextBox textBox输入密码;
        private System.Windows.Forms.TextBox textBox确认;
        private System.Windows.Forms.Label label确认;
        private System.Windows.Forms.Label label输入密码;
        private System.Windows.Forms.TextBox textBox电话;
        private System.Windows.Forms.Label label电话;
        private System.Windows.Forms.TextBox textBox身份证;
        private System.Windows.Forms.Label label身份证;
        private System.Windows.Forms.TextBox textBox姓名;
        private System.Windows.Forms.Label label姓名;
        private System.Windows.Forms.Label label开户帐户名;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Timer timer1;


    }
}