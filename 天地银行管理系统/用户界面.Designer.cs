﻿namespace 天地银行管理系统
{
    partial class 用户界面
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(用户界面));
            this.menuStrip定期 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem个人服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem重新登陆 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem挂失 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem零存整取 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem零存整取开户 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem零存整取存款 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem零存整取取款 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem零存整取查询 = new System.Windows.Forms.ToolStripMenuItem();
            this.转账ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.销号ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.投资理财ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.充值业务ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.操作记录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem系统设置 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem修改密码 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem工具 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem记事本 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem计算机 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem帮助 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem内容 = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.tmDate = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tb时间 = new System.Windows.Forms.ToolStripStatusLabel();
            this.sslbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.button7 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip定期.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip定期
            // 
            this.menuStrip定期.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem个人服务,
            this.toolStripMenuItem系统设置,
            this.toolStripMenuItem工具,
            this.toolStripMenuItem帮助});
            this.menuStrip定期.Location = new System.Drawing.Point(0, 0);
            this.menuStrip定期.Name = "menuStrip定期";
            this.menuStrip定期.Size = new System.Drawing.Size(382, 25);
            this.menuStrip定期.TabIndex = 1;
            this.menuStrip定期.Text = "menuStrip";
            // 
            // toolStripMenuItem个人服务
            // 
            this.toolStripMenuItem个人服务.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem重新登陆,
            this.toolStripMenuItem挂失,
            this.toolStripMenuItem零存整取,
            this.销号ToolStripMenuItem,
            this.投资理财ToolStripMenuItem,
            this.充值业务ToolStripMenuItem,
            this.操作记录ToolStripMenuItem});
            this.toolStripMenuItem个人服务.Name = "toolStripMenuItem个人服务";
            this.toolStripMenuItem个人服务.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
            this.toolStripMenuItem个人服务.Size = new System.Drawing.Size(82, 21);
            this.toolStripMenuItem个人服务.Text = "个人服务(&F)";
            // 
            // toolStripMenuItem重新登陆
            // 
            this.toolStripMenuItem重新登陆.Name = "toolStripMenuItem重新登陆";
            this.toolStripMenuItem重新登陆.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.L)));
            this.toolStripMenuItem重新登陆.Size = new System.Drawing.Size(191, 22);
            this.toolStripMenuItem重新登陆.Text = "重新登陆";
            this.toolStripMenuItem重新登陆.Click += new System.EventHandler(this.toolStripMenuItem重新登陆_Click);
            // 
            // toolStripMenuItem挂失
            // 
            this.toolStripMenuItem挂失.Name = "toolStripMenuItem挂失";
            this.toolStripMenuItem挂失.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.toolStripMenuItem挂失.Size = new System.Drawing.Size(191, 22);
            this.toolStripMenuItem挂失.Text = "挂失";
            this.toolStripMenuItem挂失.Click += new System.EventHandler(this.toolStripMenuItem挂失_Click);
            // 
            // toolStripMenuItem零存整取
            // 
            this.toolStripMenuItem零存整取.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem零存整取开户,
            this.toolStripMenuItem零存整取存款,
            this.toolStripMenuItem零存整取取款,
            this.toolStripMenuItem零存整取查询,
            this.转账ToolStripMenuItem});
            this.toolStripMenuItem零存整取.Name = "toolStripMenuItem零存整取";
            this.toolStripMenuItem零存整取.Size = new System.Drawing.Size(191, 22);
            this.toolStripMenuItem零存整取.Text = "零存整取";
            // 
            // toolStripMenuItem零存整取开户
            // 
            this.toolStripMenuItem零存整取开户.Name = "toolStripMenuItem零存整取开户";
            this.toolStripMenuItem零存整取开户.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem零存整取开户.Text = "用户再开户";
            this.toolStripMenuItem零存整取开户.Click += new System.EventHandler(this.toolStripMenuItem零存整取开户_Click);
            // 
            // toolStripMenuItem零存整取存款
            // 
            this.toolStripMenuItem零存整取存款.Name = "toolStripMenuItem零存整取存款";
            this.toolStripMenuItem零存整取存款.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem零存整取存款.Text = "存款";
            this.toolStripMenuItem零存整取存款.Click += new System.EventHandler(this.toolStripMenuItem零存整取存款_Click);
            // 
            // toolStripMenuItem零存整取取款
            // 
            this.toolStripMenuItem零存整取取款.Name = "toolStripMenuItem零存整取取款";
            this.toolStripMenuItem零存整取取款.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem零存整取取款.Text = "取款";
            this.toolStripMenuItem零存整取取款.Click += new System.EventHandler(this.toolStripMenuItem零存整取取款_Click);
            // 
            // toolStripMenuItem零存整取查询
            // 
            this.toolStripMenuItem零存整取查询.Name = "toolStripMenuItem零存整取查询";
            this.toolStripMenuItem零存整取查询.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem零存整取查询.Text = "余额查询";
            this.toolStripMenuItem零存整取查询.Click += new System.EventHandler(this.toolStripMenuItem零存整取查询_Click);
            // 
            // 转账ToolStripMenuItem
            // 
            this.转账ToolStripMenuItem.Name = "转账ToolStripMenuItem";
            this.转账ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.转账ToolStripMenuItem.Text = "转账";
            this.转账ToolStripMenuItem.Click += new System.EventHandler(this.转账ToolStripMenuItem_Click);
            // 
            // 销号ToolStripMenuItem
            // 
            this.销号ToolStripMenuItem.Name = "销号ToolStripMenuItem";
            this.销号ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.销号ToolStripMenuItem.Text = "销号";
            this.销号ToolStripMenuItem.Click += new System.EventHandler(this.销号ToolStripMenuItem_Click);
            // 
            // 投资理财ToolStripMenuItem
            // 
            this.投资理财ToolStripMenuItem.Name = "投资理财ToolStripMenuItem";
            this.投资理财ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.投资理财ToolStripMenuItem.Text = "投资理财";
            this.投资理财ToolStripMenuItem.Click += new System.EventHandler(this.投资理财ToolStripMenuItem_Click);
            // 
            // 充值业务ToolStripMenuItem
            // 
            this.充值业务ToolStripMenuItem.Name = "充值业务ToolStripMenuItem";
            this.充值业务ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.充值业务ToolStripMenuItem.Text = "充值业务";
            this.充值业务ToolStripMenuItem.Click += new System.EventHandler(this.充值业务ToolStripMenuItem_Click);
            // 
            // 操作记录ToolStripMenuItem
            // 
            this.操作记录ToolStripMenuItem.Name = "操作记录ToolStripMenuItem";
            this.操作记录ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.操作记录ToolStripMenuItem.Text = "操作记录";
            this.操作记录ToolStripMenuItem.Click += new System.EventHandler(this.操作记录ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem系统设置
            // 
            this.toolStripMenuItem系统设置.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem修改密码});
            this.toolStripMenuItem系统设置.Name = "toolStripMenuItem系统设置";
            this.toolStripMenuItem系统设置.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItem系统设置.Size = new System.Drawing.Size(83, 21);
            this.toolStripMenuItem系统设置.Text = "系统设置(&S)";
            // 
            // toolStripMenuItem修改密码
            // 
            this.toolStripMenuItem修改密码.Name = "toolStripMenuItem修改密码";
            this.toolStripMenuItem修改密码.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.toolStripMenuItem修改密码.Size = new System.Drawing.Size(145, 22);
            this.toolStripMenuItem修改密码.Text = "修改密码";
            this.toolStripMenuItem修改密码.Click += new System.EventHandler(this.toolStripMenuItem修改密码_Click);
            // 
            // toolStripMenuItem工具
            // 
            this.toolStripMenuItem工具.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem记事本,
            this.toolStripMenuItem计算机});
            this.toolStripMenuItem工具.Name = "toolStripMenuItem工具";
            this.toolStripMenuItem工具.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.T)));
            this.toolStripMenuItem工具.Size = new System.Drawing.Size(59, 21);
            this.toolStripMenuItem工具.Text = "工具(&T)";
            // 
            // toolStripMenuItem记事本
            // 
            this.toolStripMenuItem记事本.Name = "toolStripMenuItem记事本";
            this.toolStripMenuItem记事本.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem记事本.Text = "记事本";
            // 
            // toolStripMenuItem计算机
            // 
            this.toolStripMenuItem计算机.Name = "toolStripMenuItem计算机";
            this.toolStripMenuItem计算机.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem计算机.Text = "系统计算器";
            // 
            // toolStripMenuItem帮助
            // 
            this.toolStripMenuItem帮助.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem内容});
            this.toolStripMenuItem帮助.Name = "toolStripMenuItem帮助";
            this.toolStripMenuItem帮助.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.H)));
            this.toolStripMenuItem帮助.Size = new System.Drawing.Size(61, 21);
            this.toolStripMenuItem帮助.Text = "帮助(&H)";
            // 
            // toolStripMenuItem内容
            // 
            this.toolStripMenuItem内容.Name = "toolStripMenuItem内容";
            this.toolStripMenuItem内容.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.toolStripMenuItem内容.Size = new System.Drawing.Size(145, 22);
            this.toolStripMenuItem内容.Text = "帮助内容";
            this.toolStripMenuItem内容.Click += new System.EventHandler(this.toolStripMenuItem内容_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(59, 61);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "取款";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(59, 90);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "转账";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(268, 167);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "退出";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(268, 61);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "存款";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(268, 90);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 6;
            this.button5.Text = "挂失";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(59, 128);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 7;
            this.button6.Text = "余额查询";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // tmDate
            // 
            this.tmDate.Enabled = true;
            this.tmDate.Interval = 30;
            this.tmDate.Tick += new System.EventHandler(this.tmDate_Tick_1);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tb时间,
            this.sslbl});
            this.statusStrip1.Location = new System.Drawing.Point(0, 242);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(382, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tb时间
            // 
            this.tb时间.Name = "tb时间";
            this.tb时间.Size = new System.Drawing.Size(131, 17);
            this.tb时间.Text = "toolStripStatusLabel1";
            this.tb时间.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // sslbl
            // 
            this.sslbl.Name = "sslbl";
            this.sslbl.Size = new System.Drawing.Size(0, 17);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(268, 128);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 10;
            this.button7.Text = "投资理财";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 218);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 11;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 218);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "登录用户：";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(59, 167);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 13;
            this.button8.Text = "充值业务";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(157, 61);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 14;
            this.button9.Text = "贷款";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(157, 90);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 15;
            this.button10.Text = "还款";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(157, 128);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 16;
            this.button11.Text = "逾期还款";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(157, 167);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 17;
            this.button12.Text = "操作记录";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(190, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 19;
            this.label3.Text = "信用额度：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(273, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 18;
            this.label4.Text = "label4";
            // 
            // 用户界面
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(382, 264);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip定期);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "用户界面";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "用户界面";
            this.menuStrip定期.ResumeLayout(false);
            this.menuStrip定期.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip定期;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem个人服务;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem重新登陆;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem挂失;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem零存整取;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem零存整取开户;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem零存整取存款;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem零存整取取款;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem零存整取查询;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem系统设置;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem修改密码;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem工具;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem记事本;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem计算机;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem帮助;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem内容;
        private System.Windows.Forms.ToolStripMenuItem 转账ToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Timer tmDate;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tb时间;
        private System.Windows.Forms.ToolStripMenuItem 销号ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 投资理财ToolStripMenuItem;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripStatusLabel sslbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem 充值业务ToolStripMenuItem;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.ToolStripMenuItem 操作记录ToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}