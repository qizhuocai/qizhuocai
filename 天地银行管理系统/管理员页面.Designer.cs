﻿namespace 天地银行管理系统
{
    partial class 管理员页面
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(管理员页面));
            this.menuStrip定期 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem个人服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem重新登陆 = new System.Windows.Forms.ToolStripMenuItem();
            this.平台总余额ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户查找ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem系统设置 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem修改密码 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem工具 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem记事本 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem帮助 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem内容 = new System.Windows.Forms.ToolStripMenuItem();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.menuStrip定期.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip定期
            // 
            this.menuStrip定期.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem个人服务,
            this.toolStripMenuItem系统设置,
            this.toolStripMenuItem工具,
            this.toolStripMenuItem帮助});
            this.menuStrip定期.Location = new System.Drawing.Point(0, 0);
            this.menuStrip定期.Name = "menuStrip定期";
            this.menuStrip定期.Size = new System.Drawing.Size(365, 25);
            this.menuStrip定期.TabIndex = 16;
            this.menuStrip定期.Text = "menuStrip";
            // 
            // toolStripMenuItem个人服务
            // 
            this.toolStripMenuItem个人服务.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem重新登陆,
            this.平台总余额ToolStripMenuItem,
            this.用户查找ToolStripMenuItem,
            this.用户信息ToolStripMenuItem});
            this.toolStripMenuItem个人服务.Name = "toolStripMenuItem个人服务";
            this.toolStripMenuItem个人服务.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
            this.toolStripMenuItem个人服务.Size = new System.Drawing.Size(82, 21);
            this.toolStripMenuItem个人服务.Text = "管理服务(&F)";
            // 
            // toolStripMenuItem重新登陆
            // 
            this.toolStripMenuItem重新登陆.Name = "toolStripMenuItem重新登陆";
            this.toolStripMenuItem重新登陆.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.L)));
            this.toolStripMenuItem重新登陆.Size = new System.Drawing.Size(191, 22);
            this.toolStripMenuItem重新登陆.Text = "重新登陆";
            this.toolStripMenuItem重新登陆.Click += new System.EventHandler(this.toolStripMenuItem重新登陆_Click);
            // 
            // 平台总余额ToolStripMenuItem
            // 
            this.平台总余额ToolStripMenuItem.Name = "平台总余额ToolStripMenuItem";
            this.平台总余额ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.平台总余额ToolStripMenuItem.Text = "平台详情";
            this.平台总余额ToolStripMenuItem.Click += new System.EventHandler(this.平台总余额ToolStripMenuItem_Click);
            // 
            // 用户查找ToolStripMenuItem
            // 
            this.用户查找ToolStripMenuItem.Name = "用户查找ToolStripMenuItem";
            this.用户查找ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.用户查找ToolStripMenuItem.Text = "用户查找";
            this.用户查找ToolStripMenuItem.Click += new System.EventHandler(this.用户查找ToolStripMenuItem_Click);
            // 
            // 用户信息ToolStripMenuItem
            // 
            this.用户信息ToolStripMenuItem.Name = "用户信息ToolStripMenuItem";
            this.用户信息ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.用户信息ToolStripMenuItem.Text = "用户信息";
            this.用户信息ToolStripMenuItem.Click += new System.EventHandler(this.用户信息ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem系统设置
            // 
            this.toolStripMenuItem系统设置.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem修改密码});
            this.toolStripMenuItem系统设置.Name = "toolStripMenuItem系统设置";
            this.toolStripMenuItem系统设置.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItem系统设置.Size = new System.Drawing.Size(83, 21);
            this.toolStripMenuItem系统设置.Text = "系统设置(&S)";
            // 
            // toolStripMenuItem修改密码
            // 
            this.toolStripMenuItem修改密码.Name = "toolStripMenuItem修改密码";
            this.toolStripMenuItem修改密码.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.toolStripMenuItem修改密码.Size = new System.Drawing.Size(145, 22);
            this.toolStripMenuItem修改密码.Text = "修改密码";
            this.toolStripMenuItem修改密码.Click += new System.EventHandler(this.toolStripMenuItem修改密码_Click);
            // 
            // toolStripMenuItem工具
            // 
            this.toolStripMenuItem工具.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem记事本});
            this.toolStripMenuItem工具.Name = "toolStripMenuItem工具";
            this.toolStripMenuItem工具.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.T)));
            this.toolStripMenuItem工具.Size = new System.Drawing.Size(59, 21);
            this.toolStripMenuItem工具.Text = "工具(&T)";
            // 
            // toolStripMenuItem记事本
            // 
            this.toolStripMenuItem记事本.Name = "toolStripMenuItem记事本";
            this.toolStripMenuItem记事本.Size = new System.Drawing.Size(112, 22);
            this.toolStripMenuItem记事本.Text = "记事本";
            // 
            // toolStripMenuItem帮助
            // 
            this.toolStripMenuItem帮助.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem内容});
            this.toolStripMenuItem帮助.Name = "toolStripMenuItem帮助";
            this.toolStripMenuItem帮助.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.H)));
            this.toolStripMenuItem帮助.Size = new System.Drawing.Size(61, 21);
            this.toolStripMenuItem帮助.Text = "帮助(&H)";
            // 
            // toolStripMenuItem内容
            // 
            this.toolStripMenuItem内容.Name = "toolStripMenuItem内容";
            this.toolStripMenuItem内容.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.toolStripMenuItem内容.Size = new System.Drawing.Size(145, 22);
            this.toolStripMenuItem内容.Text = "帮助内容";
            this.toolStripMenuItem内容.Click += new System.EventHandler(this.toolStripMenuItem内容_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(33, 137);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(112, 23);
            this.button4.TabIndex = 20;
            this.button4.Text = "平台详情";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(281, 195);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 19;
            this.button3.Text = "退出";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(33, 97);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "用户信息管理";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(163, 97);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "用户查找";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(163, 137);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(112, 23);
            this.button5.TabIndex = 22;
            this.button5.Text = "平台账户";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(101, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 12);
            this.label3.TabIndex = 26;
            this.label3.Text = "今日交易金额:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(190, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 25;
            this.label4.Text = "label4";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(101, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 12);
            this.label2.TabIndex = 24;
            this.label2.Text = "平台流动资金:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(190, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 23;
            this.label1.Text = "label1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 251);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(365, 22);
            this.statusStrip1.TabIndex = 27;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(131, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 80;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 29;
            this.label5.Text = "登录名：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(104, 231);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 28;
            this.label6.Text = "label6";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(163, 169);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(112, 23);
            this.button8.TabIndex = 33;
            this.button8.Text = "用户理财信息";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(33, 166);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(112, 23);
            this.button6.TabIndex = 32;
            this.button6.Text = "用户贷款信息";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(33, 195);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(112, 23);
            this.button7.TabIndex = 31;
            this.button7.Text = "用户充值信息";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(163, 198);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(112, 23);
            this.button9.TabIndex = 34;
            this.button9.Text = "逾期还款信息";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(278, 137);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 35;
            this.button10.Text = "代客户存款";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // 管理员页面
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(365, 273);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.menuStrip定期);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "管理员页面";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "管理员页面";
            this.menuStrip定期.ResumeLayout(false);
            this.menuStrip定期.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip定期;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem个人服务;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem重新登陆;
        private System.Windows.Forms.ToolStripMenuItem 平台总余额ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户查找ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem系统设置;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem修改密码;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem工具;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem记事本;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem帮助;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem内容;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
    }
}