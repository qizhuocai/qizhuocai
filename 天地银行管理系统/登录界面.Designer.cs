﻿namespace 天地银行管理系统
{
    partial class 登录界面
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(登录界面));
            this.注册新账户 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.用户登录 = new System.Windows.Forms.Button();
            this.销号 = new System.Windows.Forms.Button();
            this.退出系统 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // 注册新账户
            // 
            this.注册新账户.Location = new System.Drawing.Point(25, 96);
            this.注册新账户.Name = "注册新账户";
            this.注册新账户.Size = new System.Drawing.Size(75, 23);
            this.注册新账户.TabIndex = 0;
            this.注册新账户.Text = "注册新账户";
            this.注册新账户.UseVisualStyleBackColor = true;
            this.注册新账户.Click += new System.EventHandler(this.注册新账户_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 19F);
            this.label1.Location = new System.Drawing.Point(38, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "天地银行登录系统";
            // 
            // 用户登录
            // 
            this.用户登录.Location = new System.Drawing.Point(169, 96);
            this.用户登录.Name = "用户登录";
            this.用户登录.Size = new System.Drawing.Size(75, 23);
            this.用户登录.TabIndex = 2;
            this.用户登录.Text = "用户登录";
            this.用户登录.UseVisualStyleBackColor = true;
            this.用户登录.Click += new System.EventHandler(this.用户登录_Click);
            // 
            // 销号
            // 
            this.销号.Location = new System.Drawing.Point(169, 171);
            this.销号.Name = "销号";
            this.销号.Size = new System.Drawing.Size(75, 23);
            this.销号.TabIndex = 3;
            this.销号.Text = "账号挂销";
            this.销号.UseVisualStyleBackColor = true;
            this.销号.Click += new System.EventHandler(this.销号_Click);
            // 
            // 退出系统
            // 
            this.退出系统.Location = new System.Drawing.Point(169, 245);
            this.退出系统.Name = "退出系统";
            this.退出系统.Size = new System.Drawing.Size(75, 23);
            this.退出系统.TabIndex = 4;
            this.退出系统.Text = "退出系统";
            this.退出系统.UseVisualStyleBackColor = true;
            this.退出系统.Click += new System.EventHandler(this.退出系统_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(25, 171);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "管理员登录";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(25, 245);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "关于软件";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // 登录界面
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(292, 327);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.退出系统);
            this.Controls.Add(this.销号);
            this.Controls.Add(this.用户登录);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.注册新账户);
            this.ForeColor = System.Drawing.Color.Red;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "登录界面";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "银行";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button 注册新账户;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button 用户登录;
        private System.Windows.Forms.Button 销号;
        private System.Windows.Forms.Button 退出系统;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

