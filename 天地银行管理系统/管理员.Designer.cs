﻿namespace 天地银行管理系统
{
    partial class 管理员
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(管理员));
            this.yonghushuju = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.quxiao = new System.Windows.Forms.Button();
            this.baocun = new System.Windows.Forms.Button();
            this.shanchu = new System.Windows.Forms.Button();
            this.xiugai = new System.Windows.Forms.Button();
            this.tianjia = new System.Windows.Forms.Button();
            this.tb确认 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb密码 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb编号 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb账号 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.yonghushuju)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // yonghushuju
            // 
            this.yonghushuju.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.yonghushuju.Dock = System.Windows.Forms.DockStyle.Fill;
            this.yonghushuju.Location = new System.Drawing.Point(0, 171);
            this.yonghushuju.Name = "yonghushuju";
            this.yonghushuju.RowTemplate.Height = 23;
            this.yonghushuju.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.yonghushuju.Size = new System.Drawing.Size(453, 174);
            this.yonghushuju.TabIndex = 2;
            this.yonghushuju.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.yonghushuju_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.quxiao);
            this.groupBox1.Controls.Add(this.baocun);
            this.groupBox1.Controls.Add(this.shanchu);
            this.groupBox1.Controls.Add(this.xiugai);
            this.groupBox1.Controls.Add(this.tianjia);
            this.groupBox1.Controls.Add(this.tb确认);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tb密码);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tb编号);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tb账号);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(453, 171);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "管理者信息";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(337, 132);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "退出";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // quxiao
            // 
            this.quxiao.Location = new System.Drawing.Point(337, 90);
            this.quxiao.Name = "quxiao";
            this.quxiao.Size = new System.Drawing.Size(75, 23);
            this.quxiao.TabIndex = 12;
            this.quxiao.Text = "操作取消";
            this.quxiao.UseVisualStyleBackColor = true;
            this.quxiao.Click += new System.EventHandler(this.quxiao_Click);
            // 
            // baocun
            // 
            this.baocun.Location = new System.Drawing.Point(337, 47);
            this.baocun.Name = "baocun";
            this.baocun.Size = new System.Drawing.Size(75, 23);
            this.baocun.TabIndex = 11;
            this.baocun.Text = "操作确认";
            this.baocun.UseVisualStyleBackColor = true;
            this.baocun.Click += new System.EventHandler(this.baocun_Click_1);
            // 
            // shanchu
            // 
            this.shanchu.Location = new System.Drawing.Point(239, 121);
            this.shanchu.Name = "shanchu";
            this.shanchu.Size = new System.Drawing.Size(75, 23);
            this.shanchu.TabIndex = 10;
            this.shanchu.Text = "删除管理者";
            this.shanchu.UseVisualStyleBackColor = true;
            this.shanchu.Click += new System.EventHandler(this.shanchu_Click_1);
            // 
            // xiugai
            // 
            this.xiugai.Location = new System.Drawing.Point(239, 68);
            this.xiugai.Name = "xiugai";
            this.xiugai.Size = new System.Drawing.Size(75, 23);
            this.xiugai.TabIndex = 9;
            this.xiugai.Text = "修改管理者";
            this.xiugai.UseVisualStyleBackColor = true;
            this.xiugai.Click += new System.EventHandler(this.xiugai_Click);
            // 
            // tianjia
            // 
            this.tianjia.Location = new System.Drawing.Point(239, 23);
            this.tianjia.Name = "tianjia";
            this.tianjia.Size = new System.Drawing.Size(75, 23);
            this.tianjia.TabIndex = 8;
            this.tianjia.Text = "添加管理者";
            this.tianjia.UseVisualStyleBackColor = true;
            this.tianjia.Click += new System.EventHandler(this.tianjia_Click);
            // 
            // tb确认
            // 
            this.tb确认.Location = new System.Drawing.Point(95, 129);
            this.tb确认.Name = "tb确认";
            this.tb确认.PasswordChar = '*';
            this.tb确认.Size = new System.Drawing.Size(100, 21);
            this.tb确认.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "确认密码";
            // 
            // tb密码
            // 
            this.tb密码.Location = new System.Drawing.Point(95, 92);
            this.tb密码.Name = "tb密码";
            this.tb密码.PasswordChar = '*';
            this.tb密码.Size = new System.Drawing.Size(100, 21);
            this.tb密码.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "密码";
            // 
            // tb编号
            // 
            this.tb编号.Location = new System.Drawing.Point(95, 65);
            this.tb编号.Name = "tb编号";
            this.tb编号.Size = new System.Drawing.Size(100, 21);
            this.tb编号.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "编号";
            // 
            // tb账号
            // 
            this.tb账号.Location = new System.Drawing.Point(95, 28);
            this.tb账号.Name = "tb账号";
            this.tb账号.Size = new System.Drawing.Size(100, 21);
            this.tb账号.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "账号";
            // 
            // 管理员
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 345);
            this.Controls.Add(this.yonghushuju);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "管理员";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "管理员";
            this.Load += new System.EventHandler(this.管理员_Load);
            ((System.ComponentModel.ISupportInitialize)(this.yonghushuju)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView yonghushuju;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button quxiao;
        private System.Windows.Forms.Button baocun;
        private System.Windows.Forms.Button shanchu;
        private System.Windows.Forms.Button xiugai;
        private System.Windows.Forms.Button tianjia;
        private System.Windows.Forms.TextBox tb确认;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb密码;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb编号;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb账号;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;

        public System.EventHandler mima_TextChanged { get; set; }
    }
}