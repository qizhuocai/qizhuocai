﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
 
namespace WindowsFormsApplication1
{
    public partial class 用户登录 : Form
    {
        public 用户登录()
        {
            InitializeComponent();
        }

        private void 登录账号_Click(object sender, EventArgs e)
        {
            string sql = "select * from 用户注册信息 where 账号='" + tb账号.Text + "'";
            DataTable datatable = sujuku.getDataFromDB(sql);   //调用DB方法返回sql
            //当用户名和密码框为空时，提示用户输入用户名和密码
            if (tb密码.Text == "" && tb账号.Text == "")
            {
                MessageBox.Show("请输入账号和密码！");
            }
            else if (tb账号.Text == "" && tb密码.Text != "")
            {
                MessageBox.Show("账号不能为空，请输入用户名！");
            }
            else if (tb账号.Text != "" && tb密码.Text == "")
            {
                MessageBox.Show("密码不能为空，请输入密码！");
            }
            else
            {
                //if (dt.Rows .Count  == 0)        //-->错误的
                if (datatable == null)                // -->正确的
                {
                    MessageBox.Show("无此账号，请检查！");
                    tb密码.Text = "";
                    tb账号.Focus();  //使鼠标焦点重新回到用户名处
                    return;
                }
                //	查询该用户是否已锁定，若是则提示“用户已锁定，请联系管理员！”，否则继续以下步骤。
                if (Convert.ToBoolean(datatable.Rows[0]["是否锁定"]) == true)
                {
                    MessageBox.Show("账号已锁定，请联系管理员！");
                    tb账号.Text = "";
                    tb密码.Text = "";
                    tb账号.Focus();  //使鼠标焦点重新回到用户名处
                    return;
                }
                if (Convert.ToBoolean(datatable.Rows[0]["挂失"]) == true)
                {
                    MessageBox.Show("该账号已经挂失，请重新恢复再登录！");
                    tb账号.Text = "";
                    tb密码.Text = "";
                    tb账号.Focus();  //使鼠标焦点重新回到用户名处
                    return;
                }
                //	查询输入的密码与数据库中该用户的密码是否匹
                if (Convert.ToString(datatable.Rows[0]["密码"]) != tb密码.Text)
                {
                    // MessageBox.Show("密码输入有误，请重新输入！");
                    //	若不匹配，显示密码错误提示信息，修改数据库密码错误次数（次数增加1），
                    //并判断错误次数是否超过最大次数（5次），若是，则锁定该用户（将数据库锁定标识位设为True），提示“用户已锁定，请联系管理员！”。
                    int i = Convert.ToInt16(datatable.Rows[0]["密码错误次数"]);  //读取数据库中的错误次数，并赋值给i
                    if (i < 6)
                    {
                        i++;
                        MessageBox.Show("密码输入有误，请重新输入！");
                        sql = "update 用户注册信息 set 密码错误次数='" + i + "'  where 账号='" + tb账号.Text + "'"; //修改数据库中的错误次数
                        sujuku.updateDB(sql);     //操纵数据库数据
                        tb密码.Text = "";
                        tb密码.Focus();
                    }
                    else
                    {
                        sql = "update 用户注册信息 set 是否锁定='" + true + "' where 账号='" + tb账号.Text + "'";
                        sujuku.updateDB(sql);
                        MessageBox.Show("账号已锁定，请联系管理员！");
                        tb密码.Text = "";
                        tb账号.Text = "";
                        tb账号.Focus();  //使鼠标焦点重新回到用户名处
                    }
                }
                else
                {
                    //	若匹配，则将数据库密码错误次数改为0
                    sql = "update 用户注册信息  set 密码错误次数=0 where 账号='" + tb账号.Text + "'";
                    sujuku.updateDB(sql);
                    //	若密码匹配，则显示主界面。
                    //CPublic.LoginSuccess = true;
                    sujuku.dl = true;
                    Close();
                }
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            用户修改密码 忘记 = new 用户修改密码();
            忘记.ShowDialog();
            Close();
        }

        private void 退出_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
