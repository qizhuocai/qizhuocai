﻿namespace 天地银行管理系统
{
    partial class dl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dl));
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.登录 = new System.Windows.Forms.Button();
            this.tb密码 = new System.Windows.Forms.TextBox();
            this.tb账号 = new System.Windows.Forms.TextBox();
            this.退出 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 11F);
            this.label4.ForeColor = System.Drawing.Color.Crimson;
            this.label4.Location = new System.Drawing.Point(190, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 27;
            this.label4.Text = "忘记密码";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 15F);
            this.label3.Location = new System.Drawing.Point(16, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 20);
            this.label3.TabIndex = 26;
            this.label3.Text = "密码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 15F);
            this.label2.Location = new System.Drawing.Point(16, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 25;
            this.label2.Text = "账号";
            // 
            // 登录
            // 
            this.登录.Location = new System.Drawing.Point(86, 130);
            this.登录.Name = "登录";
            this.登录.Size = new System.Drawing.Size(75, 23);
            this.登录.TabIndex = 24;
            this.登录.Text = "登录";
            this.登录.UseVisualStyleBackColor = true;
            this.登录.Click += new System.EventHandler(this.登录账号_Click);
            // 
            // tb密码
            // 
            this.tb密码.Location = new System.Drawing.Point(86, 80);
            this.tb密码.Name = "tb密码";
            this.tb密码.PasswordChar = '*';
            this.tb密码.Size = new System.Drawing.Size(100, 21);
            this.tb密码.TabIndex = 23;
            // 
            // tb账号
            // 
            this.tb账号.Location = new System.Drawing.Point(86, 35);
            this.tb账号.Name = "tb账号";
            this.tb账号.Size = new System.Drawing.Size(100, 21);
            this.tb账号.TabIndex = 22;
            // 
            // 退出
            // 
            this.退出.Location = new System.Drawing.Point(182, 183);
            this.退出.Name = "退出";
            this.退出.Size = new System.Drawing.Size(75, 23);
            this.退出.TabIndex = 20;
            this.退出.Text = "退出";
            this.退出.UseVisualStyleBackColor = true;
            this.退出.Click += new System.EventHandler(this.退出_Click);
            // 
            // dl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(272, 219);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.登录);
            this.Controls.Add(this.tb密码);
            this.Controls.Add(this.tb账号);
            this.Controls.Add(this.退出);
            this.ForeColor = System.Drawing.Color.DarkBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "dl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "用户登录";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button 登录;
        private System.Windows.Forms.TextBox tb密码;
        private System.Windows.Forms.TextBox tb账号;
        private System.Windows.Forms.Button 退出;
    }
}