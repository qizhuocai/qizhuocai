﻿namespace 天地银行管理系统
{
    partial class 投资理财
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.tb存入金额 = new System.Windows.Forms.TextBox();
            this.tb利率 = new System.Windows.Forms.TextBox();
            this.tb账号 = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tb时间 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.la = new System.Windows.Forms.Label();
            this.tb密码 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tzi1 = new System.Windows.Forms.RadioButton();
            this.tzi3 = new System.Windows.Forms.RadioButton();
            this.tzi2 = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.tb项目 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(191, 340);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb存入金额
            // 
            this.tb存入金额.Location = new System.Drawing.Point(113, 192);
            this.tb存入金额.Name = "tb存入金额";
            this.tb存入金额.Size = new System.Drawing.Size(142, 21);
            this.tb存入金额.TabIndex = 1;
            // 
            // tb利率
            // 
            this.tb利率.Location = new System.Drawing.Point(113, 228);
            this.tb利率.Name = "tb利率";
            this.tb利率.Size = new System.Drawing.Size(142, 21);
            this.tb利率.TabIndex = 2;
            // 
            // tb账号
            // 
            this.tb账号.Location = new System.Drawing.Point(113, 265);
            this.tb账号.Name = "tb账号";
            this.tb账号.Size = new System.Drawing.Size(142, 21);
            this.tb账号.TabIndex = 3;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tb时间});
            this.statusStrip1.Location = new System.Drawing.Point(0, 376);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(361, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tb时间
            // 
            this.tb时间.Name = "tb时间";
            this.tb时间.Size = new System.Drawing.Size(131, 17);
            this.tb时间.Text = "toolStripStatusLabel1";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "投资项目";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 228);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "利率";
            // 
            // la
            // 
            this.la.AutoSize = true;
            this.la.Location = new System.Drawing.Point(58, 192);
            this.la.Name = "la";
            this.la.Size = new System.Drawing.Size(29, 12);
            this.la.TabIndex = 8;
            this.la.Text = "金额";
            // 
            // tb密码
            // 
            this.tb密码.Location = new System.Drawing.Point(113, 292);
            this.tb密码.Name = "tb密码";
            this.tb密码.PasswordChar = '*';
            this.tb密码.Size = new System.Drawing.Size(142, 21);
            this.tb密码.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 268);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "账号";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 292);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "密码";
            // 
            // tzi1
            // 
            this.tzi1.AutoSize = true;
            this.tzi1.Checked = true;
            this.tzi1.Location = new System.Drawing.Point(15, 14);
            this.tzi1.Name = "tzi1";
            this.tzi1.Size = new System.Drawing.Size(71, 16);
            this.tzi1.TabIndex = 12;
            this.tzi1.TabStop = true;
            this.tzi1.Text = "农行货币";
            this.tzi1.UseVisualStyleBackColor = true;
            // 
            // tzi3
            // 
            this.tzi3.AutoSize = true;
            this.tzi3.Location = new System.Drawing.Point(15, 61);
            this.tzi3.Name = "tzi3";
            this.tzi3.Size = new System.Drawing.Size(71, 16);
            this.tzi3.TabIndex = 13;
            this.tzi3.TabStop = true;
            this.tzi3.Text = "天地投资";
            this.tzi3.UseVisualStyleBackColor = true;
            // 
            // tzi2
            // 
            this.tzi2.AutoSize = true;
            this.tzi2.Location = new System.Drawing.Point(15, 39);
            this.tzi2.Name = "tzi2";
            this.tzi2.Size = new System.Drawing.Size(71, 16);
            this.tzi2.TabIndex = 14;
            this.tzi2.TabStop = true;
            this.tzi2.Text = "工行货币";
            this.tzi2.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 16;
            this.label5.Text = "投资项目";
            // 
            // tb项目
            // 
            this.tb项目.Location = new System.Drawing.Point(113, 156);
            this.tb项目.Name = "tb项目";
            this.tb项目.Size = new System.Drawing.Size(142, 21);
            this.tb项目.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(47, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(317, 24);
            this.label6.TabIndex = 17;
            this.label6.Text = "温馨提示：为了更好测试自动回款，没有显示一年，半年，\r\n一个月的时间。只是用分钟来演示";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton3);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Location = new System.Drawing.Point(260, 48);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(89, 89);
            this.panel1.TabIndex = 21;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(19, 64);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(59, 16);
            this.radioButton3.TabIndex = 23;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "三分钟";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(19, 41);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(59, 16);
            this.radioButton2.TabIndex = 22;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "五分钟";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(19, 11);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(59, 16);
            this.radioButton1.TabIndex = 21;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "十分钟";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tzi1);
            this.panel2.Controls.Add(this.tzi3);
            this.panel2.Controls.Add(this.tzi2);
            this.panel2.Location = new System.Drawing.Point(90, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(154, 89);
            this.panel2.TabIndex = 22;
            // 
            // 投资理财
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 398);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb项目);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb密码);
            this.Controls.Add(this.la);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tb账号);
            this.Controls.Add(this.tb利率);
            this.Controls.Add(this.tb存入金额);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "投资理财";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "投资理财";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tb存入金额;
        private System.Windows.Forms.TextBox tb利率;
        private System.Windows.Forms.TextBox tb账号;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tb时间;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label la;
        private System.Windows.Forms.TextBox tb密码;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton tzi1;
        private System.Windows.Forms.RadioButton tzi3;
        private System.Windows.Forms.RadioButton tzi2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb项目;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Panel panel2;
    }
}