﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using 天地银行管理系统;


///
///手机充值业务
///
namespace 天地银行管理系统
{
    public partial class 充值业务 : Form
    {
        public 充值业务()
        {
            InitializeComponent();
            tb账号.Text = 用户界面.nameuser;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double 金额 = Convert.ToDouble(tb存入金额.Text);
            if (金额 < 10)
            {
                MessageBox.Show("不能少于10");
                tb存入金额.Focus();
                return;
            }

            if (tb存入金额.Text == "")
            {
                MessageBox.Show("请输入要充值的金额！");
                // tb存入金额 .Text = 
                tb存入金额.Focus();
                return;
            }
            if (tb手机号.Text == "")
            {
                MessageBox.Show("请输入要充值的手机号码！");
                // tb存入金额 .Text = 
                tb手机号.Focus();
                return;
            }
            Regex b = new Regex("^[0-9]{7,12}$");
            if (!b.IsMatch(tb手机号.Text))
            {
                tb手机号.Clear();
                tb手机号.Focus();
                MessageBox.Show("手机或固话格式输入错误，应为7-11位数字!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Question);
                return;
            } 
            if (MessageBox.Show("是否充值", "天地银行", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
            string sql = "select * from 用户金额 where 账号='" + tb账号.Text + "'";
            DataTable datatable = sujuku.getDataFromDB(sql);   //调用DB方法返回sql
            //当用户名和密码框为空时，提示用户输入用户名和密码
            登录验证 dlyz = new 登录验证();
            if (dlyz.dlan(tb账号.Text, tb密码.Text).ToString() == tb账号.Text)
            {
                //	若密码匹配，则显示主界面。


                //  SqlCommand cmd = new SqlCommand();
               
                if (金额 > 0)
                {

                    string a = 登录验证.guan(tb账号.Text, "剩余金额");
                    tb密码.Text = "";
                    //  tb账号.Focus();
                    tb密码.Focus();
                    double ts = Convert.ToDouble(a) - Convert.ToDouble(tb存入金额.Text);
                    // d.Text = ts.ToString();


                    if (ts < 100)
                    {
                        MessageBox.Show("充值失败，充值后您的余额不能低于100元，请重新输入金额");
                        tb密码.Text = "";
                        tb存入金额.Text = "";
                        d.Text = "";
                    }
                    else if (ts >= 100)
                    {
                       
                            string chongzhi = "select * from 交易记录 ";
                            chongzhi = "insert into 交易记录 values ( '" + tb账号.Text + "','" + "充值" + "','" + tb存入金额.Text + "','" + DateTime.Now.ToString() + "','" + "充值号码：" + tb手机号.Text + "')";
                            sujuku.updateDB(chongzhi);

                            string zhi = "select * from 客户充值 ";
                            zhi  = "insert into 客户充值 values ( '" + tb账号.Text + "','" + tb手机号.Text + "','" + tb存入金额.Text + "','" + tb时间.Text + "',0)";
                            sujuku.updateDB(zhi);

                                string dj = "select * from 充值冻结 ";
                                dj = "insert into 充值冻结 values ( '" + tb账号.Text + "','" + tb手机号.Text + "','" + tb存入金额.Text + "','" + tb时间.Text + "',0)";
                                sujuku.updateDB(dj);
                         //   登录验证.交易记录(tb账号.Text.ToString(), "充值", tb存入金额.Text.ToString(), tb时间.Text.ToString(), "充值号码:" + tb手机号.Text.ToString());
                          //string tsgfd="充值号码"+tb手机号 .Text; 

                            string xingx = "充值成功，充值号码：" + tb手机号.Text + "充值的金额为" + tb存入金额.Text;
                
                                MessageBox.Show(xingx+",十分钟内充值到手机上");
                                return;
                        }
                        else
                        {
                            MessageBox.Show("充值不成功，请重新操作");
                            tb密码.Text = "";
                         
                            tb存入金额.Focus();
                            return;
                        }
                    }
              

                else
                {
                    MessageBox.Show("充值不成功，请重新操作");
                    tb密码.Text = "";
                  
                    tb存入金额.Focus();
                    return;
                }
            }}
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //if (登录验证.退出(0) == 1)
            //{
            //    this.Close();
            //}

            登录验证.退出1(this);
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            tb时间.Text = DateTime.Now.ToString();


        }

        private void button3_Click(object sender, EventArgs e)
        {
            int rowindex = this.dataGridView1.CurrentRow.Index; //获取选中的行
            if (rowindex > -1)
            {
                string user = this.dataGridView1[1, rowindex].Value.ToString();
                string id = this.dataGridView1[0, rowindex].Value.ToString();
                string time = this.dataGridView1[4, rowindex].Value.ToString();
                string jine = this.dataGridView1[3, rowindex].Value.ToString();

                if (MessageBox.Show("是否退回", "天地银行", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string s = "select * from 交易记录 ";
                    s= "insert into 交易记录 values ( '" + user  + "','" + "退回充值" + "','" + jine + "','" + DateTime .Now .ToString () + "','" + id  + "')";
                    if (sujuku.updateDB(s) == true)
                    { MessageBox.Show("退回充值"); return; }
                    else { MessageBox.Show("退回不成功"); return; }
                }
            }
            else { MessageBox.Show("没有待退回信息"); }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string sql = "select * from 客户充值 where 账号 = '" +用户界面.nameuser   + "'";
            DataTable dt = sujuku.getDataFromDB(sql);
            if (dt == null)
            {
                MessageBox.Show("没有充值信息！");
                return;
            }
            dataGridView1.DataSource = dt;
        }
    }
}

