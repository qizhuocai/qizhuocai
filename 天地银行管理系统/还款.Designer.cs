﻿namespace 天地银行管理系统
{
    partial class 还款
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.选中DataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.类型DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.到期时间DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.贷款金额DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.本息DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.利息DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.贷款时间DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.贷款BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.shixunDataSet1 = new 天地银行管理系统.shixunDataSet1();
            this.shixunDataSet = new 天地银行管理系统.shixunDataSet();
            this.贷款TableAdapter = new 天地银行管理系统.shixunDataSet1TableAdapters.贷款TableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.贷款BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shixunDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shixunDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.选中DataGridViewCheckBoxColumn,
            this.类型DataGridViewTextBoxColumn,
            this.到期时间DataGridViewTextBoxColumn,
            this.贷款金额DataGridViewTextBoxColumn,
            this.本息DataGridViewTextBoxColumn,
            this.利息DataGridViewTextBoxColumn,
            this.贷款时间DataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.贷款BindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 108);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(436, 150);
            this.dataGridView1.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(335, 55);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "确定还款";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(156, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "本次还款：";
            // 
            // 选中DataGridViewCheckBoxColumn
            // 
            this.选中DataGridViewCheckBoxColumn.DataPropertyName = "选中";
            this.选中DataGridViewCheckBoxColumn.HeaderText = "选中";
            this.选中DataGridViewCheckBoxColumn.Name = "选中DataGridViewCheckBoxColumn";
            this.选中DataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // 类型DataGridViewTextBoxColumn
            // 
            this.类型DataGridViewTextBoxColumn.DataPropertyName = "类型";
            this.类型DataGridViewTextBoxColumn.HeaderText = "类型";
            this.类型DataGridViewTextBoxColumn.Name = "类型DataGridViewTextBoxColumn";
            this.类型DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 到期时间DataGridViewTextBoxColumn
            // 
            this.到期时间DataGridViewTextBoxColumn.DataPropertyName = "到期时间";
            this.到期时间DataGridViewTextBoxColumn.HeaderText = "到期时间";
            this.到期时间DataGridViewTextBoxColumn.Name = "到期时间DataGridViewTextBoxColumn";
            this.到期时间DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 贷款金额DataGridViewTextBoxColumn
            // 
            this.贷款金额DataGridViewTextBoxColumn.DataPropertyName = "贷款金额";
            this.贷款金额DataGridViewTextBoxColumn.HeaderText = "贷款金额";
            this.贷款金额DataGridViewTextBoxColumn.Name = "贷款金额DataGridViewTextBoxColumn";
            this.贷款金额DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 本息DataGridViewTextBoxColumn
            // 
            this.本息DataGridViewTextBoxColumn.DataPropertyName = "本息";
            this.本息DataGridViewTextBoxColumn.HeaderText = "本息";
            this.本息DataGridViewTextBoxColumn.Name = "本息DataGridViewTextBoxColumn";
            this.本息DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 利息DataGridViewTextBoxColumn
            // 
            this.利息DataGridViewTextBoxColumn.DataPropertyName = "利息";
            this.利息DataGridViewTextBoxColumn.HeaderText = "利息";
            this.利息DataGridViewTextBoxColumn.Name = "利息DataGridViewTextBoxColumn";
            this.利息DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 贷款时间DataGridViewTextBoxColumn
            // 
            this.贷款时间DataGridViewTextBoxColumn.DataPropertyName = "贷款时间";
            this.贷款时间DataGridViewTextBoxColumn.HeaderText = "贷款时间";
            this.贷款时间DataGridViewTextBoxColumn.Name = "贷款时间DataGridViewTextBoxColumn";
            this.贷款时间DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // 贷款BindingSource
            // 
            this.贷款BindingSource.DataMember = "贷款";
            this.贷款BindingSource.DataSource = this.shixunDataSet1;
            // 
            // shixunDataSet1
            // 
            this.shixunDataSet1.DataSetName = "shixunDataSet1";
            this.shixunDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // shixunDataSet
            // 
            this.shixunDataSet.DataSetName = "shixunDataSet";
            this.shixunDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // 贷款TableAdapter
            // 
            this.贷款TableAdapter.ClearBeforeFill = true;
            // 
            // 还款
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 270);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dataGridView1);
            this.Name = "还款";
            this.Text = "还款";
            this.Load += new System.EventHandler(this.还款_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.贷款BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shixunDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shixunDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
        private 天地银行管理系统.shixunDataSet shixunDataSet;
      //  private 网上银行系统.shixunDataSetTableAdapters.贷款TableAdapter 贷款TableAdapter;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private 天地银行管理系统.shixunDataSet1 shixunDataSet1;
        private System.Windows.Forms.BindingSource 贷款BindingSource;
        private 天地银行管理系统.shixunDataSet1TableAdapters.贷款TableAdapter 贷款TableAdapter;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 选中DataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 到期时间DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 贷款金额DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 本息DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 利息DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 贷款时间DataGridViewTextBoxColumn;
    }
}