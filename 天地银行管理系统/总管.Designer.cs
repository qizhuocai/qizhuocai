﻿namespace 天地银行管理系统
{
    partial class 总管
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(总管));
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip定期 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem个人服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem重新登陆 = new System.Windows.Forms.ToolStripMenuItem();
            this.平台总余额ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户查找ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.管理员信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem系统设置 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem个人信息 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem修改密码 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem工具 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem记事本 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem帮助 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem内容 = new System.Windows.Forms.ToolStripMenuItem();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tb时间 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.menuStrip定期.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(30, 99);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 23);
            this.button2.TabIndex = 13;
            this.button2.Text = "用户信息管理";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(30, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "管理员信息管理";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip定期
            // 
            this.menuStrip定期.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem个人服务,
            this.toolStripMenuItem系统设置,
            this.toolStripMenuItem工具,
            this.toolStripMenuItem帮助});
            this.menuStrip定期.Location = new System.Drawing.Point(0, 0);
            this.menuStrip定期.Name = "menuStrip定期";
            this.menuStrip定期.Size = new System.Drawing.Size(394, 25);
            this.menuStrip定期.TabIndex = 15;
            this.menuStrip定期.Text = "menuStrip";
            // 
            // toolStripMenuItem个人服务
            // 
            this.toolStripMenuItem个人服务.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem重新登陆,
            this.平台总余额ToolStripMenuItem,
            this.用户查找ToolStripMenuItem,
            this.管理员信息ToolStripMenuItem,
            this.用户信息ToolStripMenuItem});
            this.toolStripMenuItem个人服务.Name = "toolStripMenuItem个人服务";
            this.toolStripMenuItem个人服务.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
            this.toolStripMenuItem个人服务.Size = new System.Drawing.Size(82, 21);
            this.toolStripMenuItem个人服务.Text = "个人服务(&F)";
            // 
            // toolStripMenuItem重新登陆
            // 
            this.toolStripMenuItem重新登陆.Name = "toolStripMenuItem重新登陆";
            this.toolStripMenuItem重新登陆.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.L)));
            this.toolStripMenuItem重新登陆.Size = new System.Drawing.Size(191, 22);
            this.toolStripMenuItem重新登陆.Text = "重新登陆";
            this.toolStripMenuItem重新登陆.Click += new System.EventHandler(this.toolStripMenuItem重新登陆_Click);
            // 
            // 平台总余额ToolStripMenuItem
            // 
            this.平台总余额ToolStripMenuItem.Name = "平台总余额ToolStripMenuItem";
            this.平台总余额ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.平台总余额ToolStripMenuItem.Text = "平台详情";
            this.平台总余额ToolStripMenuItem.Click += new System.EventHandler(this.平台总余额ToolStripMenuItem_Click);
            // 
            // 用户查找ToolStripMenuItem
            // 
            this.用户查找ToolStripMenuItem.Name = "用户查找ToolStripMenuItem";
            this.用户查找ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.用户查找ToolStripMenuItem.Text = "用户查找";
            this.用户查找ToolStripMenuItem.Click += new System.EventHandler(this.用户查找ToolStripMenuItem_Click);
            // 
            // 管理员信息ToolStripMenuItem
            // 
            this.管理员信息ToolStripMenuItem.Name = "管理员信息ToolStripMenuItem";
            this.管理员信息ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.管理员信息ToolStripMenuItem.Text = "管理员信息";
            this.管理员信息ToolStripMenuItem.Click += new System.EventHandler(this.管理员信息ToolStripMenuItem_Click);
            // 
            // 用户信息ToolStripMenuItem
            // 
            this.用户信息ToolStripMenuItem.Name = "用户信息ToolStripMenuItem";
            this.用户信息ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.用户信息ToolStripMenuItem.Text = "用户信息";
            this.用户信息ToolStripMenuItem.Click += new System.EventHandler(this.用户信息ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem系统设置
            // 
            this.toolStripMenuItem系统设置.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem个人信息,
            this.toolStripMenuItem修改密码});
            this.toolStripMenuItem系统设置.Name = "toolStripMenuItem系统设置";
            this.toolStripMenuItem系统设置.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItem系统设置.Size = new System.Drawing.Size(83, 21);
            this.toolStripMenuItem系统设置.Text = "系统设置(&S)";
            // 
            // toolStripMenuItem个人信息
            // 
            this.toolStripMenuItem个人信息.Name = "toolStripMenuItem个人信息";
            this.toolStripMenuItem个人信息.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.toolStripMenuItem个人信息.Size = new System.Drawing.Size(145, 22);
            this.toolStripMenuItem个人信息.Text = "管理信息";
            this.toolStripMenuItem个人信息.Click += new System.EventHandler(this.toolStripMenuItem个人信息_Click);
            // 
            // toolStripMenuItem修改密码
            // 
            this.toolStripMenuItem修改密码.Name = "toolStripMenuItem修改密码";
            this.toolStripMenuItem修改密码.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.toolStripMenuItem修改密码.Size = new System.Drawing.Size(145, 22);
            this.toolStripMenuItem修改密码.Text = "修改密码";
            this.toolStripMenuItem修改密码.Click += new System.EventHandler(this.toolStripMenuItem修改密码_Click);
            // 
            // toolStripMenuItem工具
            // 
            this.toolStripMenuItem工具.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem记事本});
            this.toolStripMenuItem工具.Name = "toolStripMenuItem工具";
            this.toolStripMenuItem工具.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.T)));
            this.toolStripMenuItem工具.Size = new System.Drawing.Size(59, 21);
            this.toolStripMenuItem工具.Text = "工具(&T)";
            // 
            // toolStripMenuItem记事本
            // 
            this.toolStripMenuItem记事本.Name = "toolStripMenuItem记事本";
            this.toolStripMenuItem记事本.Size = new System.Drawing.Size(112, 22);
            this.toolStripMenuItem记事本.Text = "记事本";
            // 
            // toolStripMenuItem帮助
            // 
            this.toolStripMenuItem帮助.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem内容});
            this.toolStripMenuItem帮助.Name = "toolStripMenuItem帮助";
            this.toolStripMenuItem帮助.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.H)));
            this.toolStripMenuItem帮助.Size = new System.Drawing.Size(61, 21);
            this.toolStripMenuItem帮助.Text = "帮助(&H)";
            // 
            // toolStripMenuItem内容
            // 
            this.toolStripMenuItem内容.Name = "toolStripMenuItem内容";
            this.toolStripMenuItem内容.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.toolStripMenuItem内容.Size = new System.Drawing.Size(145, 22);
            this.toolStripMenuItem内容.Text = "帮助内容";
            this.toolStripMenuItem内容.Click += new System.EventHandler(this.toolStripMenuItem内容_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(199, 99);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(112, 23);
            this.button4.TabIndex = 16;
            this.button4.Text = "平台详情查询";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(199, 128);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(112, 23);
            this.button5.TabIndex = 17;
            this.button5.Text = "平台账号";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tb时间});
            this.statusStrip1.Location = new System.Drawing.Point(0, 233);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(394, 22);
            this.statusStrip1.TabIndex = 18;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tb时间
            // 
            this.tb时间.Name = "tb时间";
            this.tb时间.Size = new System.Drawing.Size(131, 17);
            this.tb时间.Text = "toolStripStatusLabel1";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(148, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 19;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 12);
            this.label2.TabIndex = 20;
            this.label2.Text = "平台流动资金:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 22;
            this.label3.Text = "总交易金额:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(148, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 21;
            this.label4.Text = "label4";
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 10;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(30, 157);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(112, 23);
            this.button6.TabIndex = 24;
            this.button6.Text = "用户贷款信息";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(30, 186);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(112, 23);
            this.button7.TabIndex = 23;
            this.button7.Text = "用户充值信息";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(199, 160);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(112, 23);
            this.button8.TabIndex = 25;
            this.button8.Text = "用户理财信息";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(199, 189);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(112, 23);
            this.button9.TabIndex = 26;
            this.button9.Text = "逾期还款信息";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // 总管
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(394, 255);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.menuStrip定期);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "总管";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "总管";
            this.menuStrip定期.ResumeLayout(false);
            this.menuStrip定期.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip定期;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem个人服务;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem重新登陆;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem系统设置;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem个人信息;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem修改密码;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem工具;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem记事本;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem帮助;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem内容;
        private System.Windows.Forms.ToolStripMenuItem 平台总余额ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户查找ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 管理员信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户信息ToolStripMenuItem;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tb时间;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
    }
}