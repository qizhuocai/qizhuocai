﻿using System;
using System.Data;
using System.Windows.Forms;

namespace 天地银行管理系统
{
    public partial class 逾期还款 : Form
    {
        public 逾期还款()
        {
            InitializeComponent();
        }
        string name = 用户界面.nameuser;
    
        private void button1_Click(object sender, EventArgs e)
        {
            


            string sql = "select * from 贷款逾期 where 账号 = '" + name + "'";
            DataTable dt = sujuku.getDataFromDB(sql);
            if (dt == null)
            {
                MessageBox.Show("没有贷款逾期！");
                return;
            }
            dataGridView1.DataSource = dt;
      
        }

        private void button2_Click(object sender, EventArgs e)
        {
          
            if (MessageBox.Show("是否还款", "天地银行", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
                {
                    MessageBox.Show("选择贷款逾期信息");
                    return;
                }
                string sqlaa = "select * from 用户金额 where 账号 = '" + name + "'";
               
                  DataTable datatable = sujuku.getDataFromDB(sqlaa);
                  double ac= Convert.ToDouble   (datatable.Rows[0]["剩余金额"]);
           
                  if (    ac <= Convert.ToDouble(textBox1.Text))
                  {
                      MessageBox.Show("不够钱还，请充值！");
                      return;
                  }
                  string sql = "select * from 贷款逾期 where 账号 = '" + name + "'";
            DataTable dt = sujuku.getDataFromDB(sql);
            if (dt == null)
            {
                MessageBox.Show("没有还款信息！");
                return;
            }


            else
            {
                // int rowindex = this.dataGridView1.CurrentRow.Index; //获取选中的行

                //this.Validate();
                //this.dataGridView1.EndEdit();
                int rowindex = this.dataGridView1.CurrentCell.RowIndex; //选中所选行
                string de = this.dataGridView1[0, rowindex].Value.ToString();
                string dq = this.dataGridView1[2, rowindex].Value.ToString();
                //foreach (DataGridViewRow r in dataGridView1.SelectedRows)//选中的行
                //{

                //    this.dataGridView1.Rows.Remove(r);
                //    //DataRowView drv = dataGridView1.SelectedRows[0].DataBoundItem as DataRowView;

                //    //drv.Row.Delete(); 
                //}
                string SQL = "delete from 贷款逾期 where ID='" + de.ToString() + "'";
             //   sujuku.updateDB(SQL); 
                string saql = "select * from 贷款逾期 where  ID='" + de.ToString() + "'";
                DataTable dta = sujuku.getDataFromDB(saql);
                if (dta == null)
                {
                    MessageBox.Show("没有逾期信息！");
                    return;
                }
                //this.dataGridView1.Update();
              

               //     string SQL = "delete from 贷款逾期 where ID='" + de.ToString() + "'";
                    //  sujuku.updateDB(SQL); 
                    this.dataGridView1.Update();
                    if (sujuku.updateDB(SQL) == true)
                    {
                        string sqq = "select * from 交易记录 ";
                        sqq = "insert into 交易记录 values ( '" + name + "','" + "还款" + "','" + textBox1.Text + "','" + DateTime.Now.ToString() + "','" + "逾期还款：" + dq  + "')";
                        sujuku.updateDB(sqq);
                        MessageBox.Show("还款成功，因为您已经逾期，所以信用额度减半");

                    }
                    else
                    { MessageBox.Show("还款错误"); return; }
                 
         
            }
            }
        }

        public  void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = this.dataGridView1.CurrentRow.Index; //获取选中的行
            string dr = this.dataGridView1[6, rowindex].Value.ToString();
            string da = this.dataGridView1[0, rowindex].Value.ToString();
            string d = this.dataGridView1[3, rowindex].Value.ToString();
            textBox1.Text = d; textBox2.Text = dr; textBox3.Text = da;

        }

        private void button3_Click(object sender, EventArgs e)
        {
         //for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
         ////   dataGridView1.Rows.Remove(dataGridView1.SelectedRows[i]);

            this.Validate();
            this.dataGridView1.EndEdit();
            int rowindex = this.dataGridView1.CurrentCell.RowIndex; //选中所选行
          string de = this.dataGridView1[0, rowindex].Value.ToString();
            foreach (DataGridViewRow r in dataGridView1.SelectedRows)//选中的行
            {

              this.dataGridView1.Rows.Remove(r);
                //DataRowView drv = dataGridView1.SelectedRows[0].DataBoundItem as DataRowView;

                //drv.Row.Delete(); 
            }
            string SQL = "delete from 贷款逾期 where ID='" + de.ToString() + "'";
          //  sujuku.updateDB(SQL); 
            this.dataGridView1.Update();
            if (sujuku.updateDB(SQL) == true)
            {
                string sqq = "select * from 交易记录 ";
                sqq = "insert into 交易记录 values ( '" + name + "','" + "逾期还款" + "','" + textBox1.Text + "','" + DateTime.Now.ToString() + "','" + "还款账号：" + name + "')";
                sujuku.updateDB(sqq);
                MessageBox.Show("还款成功，因为您已经逾期，所以信用额度减半");

            }
            else
            { MessageBox.Show("还款错误"); return; }
          
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
