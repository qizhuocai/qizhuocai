﻿namespace WindowsFormsApplication1
{
    partial class 登录界面
    { 
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.注册新账户 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.登录 = new System.Windows.Forms.Button();
            this.销号 = new System.Windows.Forms.Button();
            this.退出系统 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // 注册新账户
            // 
            this.注册新账户.Location = new System.Drawing.Point(110, 92);
            this.注册新账户.Name = "注册新账户";
            this.注册新账户.Size = new System.Drawing.Size(75, 23);
            this.注册新账户.TabIndex = 0;
            this.注册新账户.Text = "注册新账户";
            this.注册新账户.UseVisualStyleBackColor = true;
            this.注册新账户.Click += new System.EventHandler(this.注册新账户_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 19F);
            this.label1.Location = new System.Drawing.Point(41, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "ATM网上登录系统";
            // 
            // 登录
            // 
            this.登录.Location = new System.Drawing.Point(110, 132);
            this.登录.Name = "登录";
            this.登录.Size = new System.Drawing.Size(75, 23);
            this.登录.TabIndex = 2;
            this.登录.Text = "登录";
            this.登录.UseVisualStyleBackColor = true;
            // 
            // 销号
            // 
            this.销号.Location = new System.Drawing.Point(110, 177);
            this.销号.Name = "销号";
            this.销号.Size = new System.Drawing.Size(75, 23);
            this.销号.TabIndex = 3;
            this.销号.Text = "销号";
            this.销号.UseVisualStyleBackColor = true;
            // 
            // 退出系统
            // 
            this.退出系统.Location = new System.Drawing.Point(110, 206);
            this.退出系统.Name = "退出系统";
            this.退出系统.Size = new System.Drawing.Size(75, 23);
            this.退出系统.TabIndex = 4;
            this.退出系统.Text = "退出系统";
            this.退出系统.UseVisualStyleBackColor = true;
            // 
            // 登录界面
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.退出系统);
            this.Controls.Add(this.销号);
            this.Controls.Add(this.登录);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.注册新账户);
            this.Name = "登录界面";
            this.Text = "ATM网上登录系统";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button 注册新账户;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button 登录;
        private System.Windows.Forms.Button 销号;
        private System.Windows.Forms.Button 退出系统;
    }
}

