﻿namespace 天地银行管理系统
{
    partial class 注册新帐户
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(注册新帐户));
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tb现居地 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb姓名 = new System.Windows.Forms.TextBox();
            this.label确认 = new System.Windows.Forms.Label();
            this.label输入密码 = new System.Windows.Forms.Label();
            this.label电话 = new System.Windows.Forms.Label();
            this.tb身份证 = new System.Windows.Forms.TextBox();
            this.label身份证 = new System.Windows.Forms.Label();
            this.tb开户金额 = new System.Windows.Forms.TextBox();
            this.tb密码 = new System.Windows.Forms.TextBox();
            this.tb联系电话 = new System.Windows.Forms.TextBox();
            this.tb确认密码 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tb男 = new System.Windows.Forms.RadioButton();
            this.tb女 = new System.Windows.Forms.RadioButton();
            this.tb账号 = new System.Windows.Forms.TextBox();
            this.tb点击获取账号名 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tt时间 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(399, 326);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 16;
            this.button8.Text = "退出";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(310, 326);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 17;
            this.button9.Text = "确认注册";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 19F);
            this.label1.Location = new System.Drawing.Point(170, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 26);
            this.label1.TabIndex = 19;
            this.label1.Text = "欢迎加入";
            // 
            // tb现居地
            // 
            this.tb现居地.Location = new System.Drawing.Point(120, 180);
            this.tb现居地.Name = "tb现居地";
            this.tb现居地.Size = new System.Drawing.Size(139, 21);
            this.tb现居地.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 30;
            this.label3.Text = "现居地";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(265, 151);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 29;
            this.label2.Text = "性别";
            // 
            // tb姓名
            // 
            this.tb姓名.Location = new System.Drawing.Point(120, 105);
            this.tb姓名.Name = "tb姓名";
            this.tb姓名.Size = new System.Drawing.Size(139, 21);
            this.tb姓名.TabIndex = 22;
            // 
            // label确认
            // 
            this.label确认.AutoSize = true;
            this.label确认.Location = new System.Drawing.Point(265, 221);
            this.label确认.Name = "label确认";
            this.label确认.Size = new System.Drawing.Size(59, 12);
            this.label确认.TabIndex = 28;
            this.label确认.Text = "确认密码*";
            // 
            // label输入密码
            // 
            this.label输入密码.AutoSize = true;
            this.label输入密码.Location = new System.Drawing.Point(265, 185);
            this.label输入密码.Name = "label输入密码";
            this.label输入密码.Size = new System.Drawing.Size(59, 12);
            this.label输入密码.TabIndex = 27;
            this.label输入密码.Text = "输入密码*";
            // 
            // label电话
            // 
            this.label电话.AutoSize = true;
            this.label电话.Location = new System.Drawing.Point(265, 111);
            this.label电话.Name = "label电话";
            this.label电话.Size = new System.Drawing.Size(59, 12);
            this.label电话.TabIndex = 26;
            this.label电话.Text = "联系电话*";
            // 
            // tb身份证
            // 
            this.tb身份证.Location = new System.Drawing.Point(120, 220);
            this.tb身份证.Name = "tb身份证";
            this.tb身份证.Size = new System.Drawing.Size(139, 21);
            this.tb身份证.TabIndex = 24;
            // 
            // label身份证
            // 
            this.label身份证.AutoSize = true;
            this.label身份证.Location = new System.Drawing.Point(63, 224);
            this.label身份证.Name = "label身份证";
            this.label身份证.Size = new System.Drawing.Size(47, 12);
            this.label身份证.TabIndex = 25;
            this.label身份证.Text = "身份证*";
            // 
            // tb开户金额
            // 
            this.tb开户金额.Location = new System.Drawing.Point(120, 142);
            this.tb开户金额.Name = "tb开户金额";
            this.tb开户金额.Size = new System.Drawing.Size(139, 21);
            this.tb开户金额.TabIndex = 23;
            // 
            // tb密码
            // 
            this.tb密码.Location = new System.Drawing.Point(325, 180);
            this.tb密码.Name = "tb密码";
            this.tb密码.PasswordChar = '*';
            this.tb密码.Size = new System.Drawing.Size(139, 21);
            this.tb密码.TabIndex = 35;
            // 
            // tb联系电话
            // 
            this.tb联系电话.Location = new System.Drawing.Point(325, 105);
            this.tb联系电话.Name = "tb联系电话";
            this.tb联系电话.Size = new System.Drawing.Size(139, 21);
            this.tb联系电话.TabIndex = 32;
            // 
            // tb确认密码
            // 
            this.tb确认密码.Location = new System.Drawing.Point(325, 220);
            this.tb确认密码.Name = "tb确认密码";
            this.tb确认密码.PasswordChar = '*';
            this.tb确认密码.Size = new System.Drawing.Size(139, 21);
            this.tb确认密码.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 36;
            this.label4.Text = "姓名*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(63, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 37;
            this.label5.Text = "开户金额";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(63, 264);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 38;
            this.label6.Text = "账号*";
            // 
            // tb男
            // 
            this.tb男.AutoSize = true;
            this.tb男.Location = new System.Drawing.Point(325, 146);
            this.tb男.Name = "tb男";
            this.tb男.Size = new System.Drawing.Size(35, 16);
            this.tb男.TabIndex = 39;
            this.tb男.TabStop = true;
            this.tb男.Text = "男";
            this.tb男.UseVisualStyleBackColor = true;
            this.tb男.Validating += new System.ComponentModel.CancelEventHandler(this.tb男_Validating);
            // 
            // tb女
            // 
            this.tb女.AutoSize = true;
            this.tb女.Location = new System.Drawing.Point(366, 147);
            this.tb女.Name = "tb女";
            this.tb女.Size = new System.Drawing.Size(35, 16);
            this.tb女.TabIndex = 40;
            this.tb女.TabStop = true;
            this.tb女.Text = "女";
            this.tb女.UseVisualStyleBackColor = true;
            this.tb女.Validating += new System.ComponentModel.CancelEventHandler(this.tb女_Validating);
            // 
            // tb账号
            // 
            this.tb账号.Location = new System.Drawing.Point(120, 261);
            this.tb账号.Name = "tb账号";
            this.tb账号.Size = new System.Drawing.Size(139, 21);
            this.tb账号.TabIndex = 41;
            // 
            // tb点击获取账号名
            // 
            this.tb点击获取账号名.Location = new System.Drawing.Point(282, 259);
            this.tb点击获取账号名.Name = "tb点击获取账号名";
            this.tb点击获取账号名.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tb点击获取账号名.Size = new System.Drawing.Size(103, 23);
            this.tb点击获取账号名.TabIndex = 45;
            this.tb点击获取账号名.Text = "点击获取账号";
            this.tb点击获取账号名.UseVisualStyleBackColor = true;
            this.tb点击获取账号名.Click += new System.EventHandler(this.tb点击获取账号名_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tt时间});
            this.statusStrip1.Location = new System.Drawing.Point(0, 372);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(506, 22);
            this.statusStrip1.TabIndex = 46;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tt时间
            // 
            this.tt时间.Name = "tt时间";
            this.tt时间.Size = new System.Drawing.Size(32, 17);
            this.tt时间.Text = "时间";
            // 
            // 注册新帐户
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 394);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tb点击获取账号名);
            this.Controls.Add(this.tb账号);
            this.Controls.Add(this.tb女);
            this.Controls.Add(this.tb男);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb密码);
            this.Controls.Add(this.tb联系电话);
            this.Controls.Add(this.tb确认密码);
            this.Controls.Add(this.tb现居地);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb姓名);
            this.Controls.Add(this.label确认);
            this.Controls.Add(this.label输入密码);
            this.Controls.Add(this.label电话);
            this.Controls.Add(this.tb身份证);
            this.Controls.Add(this.label身份证);
            this.Controls.Add(this.tb开户金额);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "注册新帐户";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "注册新账户";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb现居地;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb姓名;
        private System.Windows.Forms.Label label确认;
        private System.Windows.Forms.Label label输入密码;
        private System.Windows.Forms.Label label电话;
        private System.Windows.Forms.TextBox tb身份证;
        private System.Windows.Forms.Label label身份证;
        private System.Windows.Forms.TextBox tb开户金额;
        private System.Windows.Forms.TextBox tb密码;
        private System.Windows.Forms.TextBox tb联系电话;
        private System.Windows.Forms.TextBox tb确认密码;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton tb男;
        private System.Windows.Forms.RadioButton tb女;
        private System.Windows.Forms.TextBox tb账号;
        private System.Windows.Forms.Button tb点击获取账号名;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tt时间;

        public System.EventHandler 注册新帐户_Load { get; set; }

        public System.EventHandler toolStripStatusLabel1_Click { get; set; }
    }
}