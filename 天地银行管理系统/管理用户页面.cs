﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace 天地银行管理系统
{
    public partial class 管理页面 : Form
    {
        public 管理页面()
        {
            InitializeComponent();
        }

        SqlConnection sql = new SqlConnection();
        string line = "Data Source=.;Initial Catalog=shixun;Integrated Security=True";
        SqlCommand cmd = new SqlCommand();
       // string imagefile = "";

        private void button1_Click_1(object sender, EventArgs e)
        {

            if (tb账号.Text == string.Empty || tb姓名.Text == string.Empty)
            {
                MessageBox.Show("请完善用户信息再注册");
                return;
            }
            string s;
            DataTable dt = new DataTable();
            sql.ConnectionString = line;
            sql.Open();
            cmd.Connection = sql;
            s = "select *from 用户注册信息 where 账号='" + tb账号.Text + "'";
            SqlDataAdapter ad = new SqlDataAdapter(s, sql);
            ad.Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                MessageBox.Show("该用户已存在");
                sql.Close();
                return;
            }
            else
            {
                cmd.CommandText = "insert into 用户注册信息 values ( @账号,@密码,@姓名,@联系电话,@开户金额,@性别,@现居地,@开户日期,@身份证,@密码错误次数,@是否锁定,@挂失)";
                cmd.Parameters.Add("@账号", SqlDbType.Char, 8).Value = tb账号.Text;
                cmd.Parameters.Add("@密码", SqlDbType.VarChar   , 128).Value = "666666";
                cmd.Parameters.Add("@姓名", SqlDbType.Char, 8).Value = tb姓名.Text;
                cmd.Parameters.Add("@联系电话", SqlDbType.Char, 15).Value = tb联系电话.Text;
                cmd.Parameters.Add("@开户金额", SqlDbType .Money ).Value = tb开户金额.Text ;
                if (nan.Checked)
                    cmd.Parameters.Add("@性别", SqlDbType.Bit).Value = 1;
                if (nv.Checked)
                    cmd.Parameters.Add("@性别", SqlDbType.Bit).Value = 2;
                cmd.Parameters.Add("@现居地", SqlDbType.VarChar,128).Value = tb现居地.Text;

                cmd.Parameters.Add("@开户日期", SqlDbType.SmallDateTime).Value = shijian.Value.Date;
                cmd.Parameters.Add("@身份证", SqlDbType.Char,20).Value = tb身份证.Text;

                cmd.Parameters.Add("@密码错误次数", SqlDbType.SmallInt  ).Value =0;
                cmd.Parameters.Add("@是否锁定", SqlDbType.Bit ).Value = false ;
                cmd.Parameters.Add("@挂失", SqlDbType.Bit ).Value = false  ;
                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("注册成功");
                    string Strsql;
                    Strsql = "select * from  用户注册信息";
                    cmd.CommandText = Strsql;
                    cmd.ExecuteNonQuery();
                    SqlDataAdapter Adapter = new SqlDataAdapter(Strsql, sql);
                    DataTable table = new DataTable();
                    Adapter.Fill(table);
                    shujuku.DataSource = table;
                    sql.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("注册失败" + ex.Message);
                    sql.Close();
                }
            }
         string    sa = "select * from  用户注册信息";
         sujuku.updateDB(sa );
            //	        刷新GridView，重新显示所有读者资料。
            
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (tb账号.Text != "")
            {

                sql.ConnectionString = line;
                sql.Open();
                string s;
                s = "delete from 用户注册信息  where 账号='" + tb账号.Text + "'";
                cmd.CommandText = s;
                cmd.Connection = sql;
                try
                {
                    DialogResult result = MessageBox.Show("确定要删除该用户信息吗？", "天地银行", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        cmd.ExecuteNonQuery();
                        sql.Close();
                        MessageBox.Show("删除成功");
                        string sa = "select * from  用户注册信息";
                        sujuku.updateDB(sa);
                    }
                    else
                        sql.Close();
                }
                catch
                {
                    sql.Close();
                }
            }
            MessageBox.Show("没有可删除用户");
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (tb账号.Text != "")
            {
                //string s;
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sql;
                sql.ConnectionString = line;
                sql.Open();
                cmd.Connection = sql;


                cmd.CommandText = "update 用户注册信息 set 密码错误次数=@密码错误次数,是否锁定=@是否锁定,挂失=@挂失, 姓名=@姓名,性别=@性别,联系电话=@联系电话,开户金额=@开户金额,现居地=@现居地,开户日期=@开户日期 , 身份证=@身份证 where 账号=@账号";
                //  cmd.CommandText = "insert into 用户注册信息 values(@账号,@密码，@姓名,@联系电话,@开户金额,@性别,@现居地,@开户日期,@身份证)";
                cmd.Parameters.Add("@账号", SqlDbType.Char, 8).Value = tb账号.Text;
                cmd.Parameters.Add("@密码", SqlDbType.VarChar, 128).Value = tb密码.Text;
                cmd.Parameters.Add("@姓名", SqlDbType.Char, 8).Value = tb姓名.Text;
                cmd.Parameters.Add("@联系电话", SqlDbType.Char, 15).Value = tb联系电话.Text;
                cmd.Parameters.Add("@开户金额", SqlDbType.Money).Value = tb开户金额.Text;
                if (nan.Checked)
                    cmd.Parameters.Add("@性别", SqlDbType.Bit).Value = 1;
                if (nv.Checked)
                    cmd.Parameters.Add("@性别", SqlDbType.Bit).Value = 2;
                cmd.Parameters.Add("@现居地", SqlDbType.VarChar, 128).Value = tb现居地.Text;

                cmd.Parameters.Add("@开户日期", SqlDbType.SmallDateTime).Value = shijian.Value.Date;
                cmd.Parameters.Add("@身份证", SqlDbType.Char, 20).Value = tb身份证.Text;
                cmd.Parameters.Add("@密码错误次数", SqlDbType.SmallInt).Value = 0;
                cmd.Parameters.Add("@是否锁定", SqlDbType.Bit).Value = "false";
                cmd.Parameters.Add("@挂失", SqlDbType.Bit).Value = "false";

                try
                {
                    //cmd.CommandText = s;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("修改用户成功");
                    string sa = "select * from  用户注册信息";
                    sujuku.updateDB(sa);
                    sql.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("修改用户失败" + ex.Message);
                    sql.Close();
                }
            } MessageBox.Show("没有可修改用户");
        }
        private void button4_Click_1(object sender, EventArgs e)
        {

            sql.ConnectionString = line;
            sql.Open();
            string s;
            if (tb账号.Text != string.Empty)
            {
                s = "select * from 用户注册信息 where 账号= '" + tb账号.Text + "'";
              //  a = "select * from 用户注册信息 where 姓名='" + tb姓名.Text + "'";
            }
            else
             s = "select * from 用户注册信息";
            cmd.CommandText = s;
            cmd.Connection = sql;
            cmd.ExecuteNonQuery();
            SqlDataAdapter Adapter = new SqlDataAdapter(s, sql);
            DataTable table = new DataTable();
            Adapter.Fill(table);
            shujuku.DataSource = table;
            if (table.Rows.Count == 0)
                MessageBox.Show("该用户不存在");
           sql.Close();
        }

        private void shujuku_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                tb账号.Text = shujuku.CurrentRow.Cells[1].Value.ToString();
                tb姓名.Text = shujuku.CurrentRow.Cells[3].Value.ToString();
              
                tb联系电话.Text = shujuku.CurrentRow.Cells[4].Value.ToString();
                tb开户金额.Text = shujuku.CurrentRow.Cells[5].Value.ToString();
                tb现居地.Text = shujuku.CurrentRow.Cells[7].Value.ToString();
                shijian.Value = (DateTime)shujuku.CurrentRow.Cells[8].Value;
                tb身份证.Text = shujuku.CurrentRow.Cells[9].Value.ToString();
                tb密码.Text = shujuku.CurrentRow.Cells[2].Value.ToString();
                if ((bool)shujuku.CurrentRow.Cells[6].Value == true)
                    nan.Checked = true;
                else
                    nv.Checked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void 管理页面_Load(object sender, EventArgs e)
        {
            tb开户金额.Enabled = false;
          //  tb密码.Enabled = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    


     
      
     

        
   

     
   



   

      
      

  
     
      
    }
}
